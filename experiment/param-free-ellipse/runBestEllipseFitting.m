%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Simple implementation (not speed optimized) of AEFA, DEFA and EMAR methods proposed in [1]
%[1] C. Panagiotakis and A. Argyros, Parameter-free Modelling of 2D Shapes with Ellipses, Pattern Recognition, 2015 
%
% You can download the datasets used in [1] from
% https://sites.google.com/site/costaspanagiotakis/research/EFA 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all;
clear all;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Method Selection
%Set METHOD = 1 to run DEFA method, 
%Set METHOD = 2 to run AEFA method, 
%else you run EMAR method  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
timer_start = tic;
METHOD = 1; 

AICBIC_SELECTION = 1; %Set AICBIC_SELECTION = 1, to use AIC is selected else BIC is used

set(0,'DefaultFigureColormap',jet);

fname = 'im03_9.gif' %filename of test example (input binary image)
[A] = myImRead(sprintf('%s',fname),1);

%figure;
%imagesc(A);
%title('Input Image');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%The last two plots show the final result and the AIC/BIC graph for different number of ellipses, respectively.   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if METHOD == 1,
    [IClust,EL,NUMEllipses] = runMergeFitting(A,AICBIC_SELECTION);%DEFA method
elseif METHOD == 2,
    [IClust,EL,NUMEllipses] = runSlitFitting(A,AICBIC_SELECTION);%AEFA method
else
    [IClust,EL,NUMEllipses] = runGMMFitting(A,AICBIC_SELECTION);%EMAR method 
end
close all;
   
elapsed = toc(timer_start)

%figure; imagesc(A); hold on;
[y_A, x_A] = find(A == 1);
yx_A = [y_A, x_A];
accum_X = []; accum_Y = [];
for i = 1:numel(EL)
  X0 = EL(i).C(1)-size(A, 2);
  Y0 = EL(i).C(2)-size(A, 1);
  major_axis = EL(i).a;
  minor_axis = EL(i).b;
  phi = EL(i).phi*pi/180;
  [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi, 'fill', size(A));

  % plot(X, Y, 'ko', 'MarkerSize', 5, 'MarkerFaceColor', 'k')
  accum_X = [accum_X; X];
  accum_Y = [accum_Y; Y];
end

yx_ellipse = [accum_Y, accum_X];
yx_intersect = intersect(yx_A, yx_ellipse);
yx_union = union(yx_A, yx_ellipse);
iou = numel(yx_intersect)/numel(yx_union)
