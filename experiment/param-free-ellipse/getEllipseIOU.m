function iou = getEllipseIOU(A, EL)

  [y_A, x_A] = find(A == 1);
  yx_A = [y_A, x_A];
  accum_X = []; accum_Y = [];
  for i = 1:numel(EL)
    X0 = EL(i).C(1)-size(A, 2);
    Y0 = EL(i).C(2)-size(A, 1);
    major_axis = EL(i).a;
    minor_axis = EL(i).b;
    phi = EL(i).phi*pi/180;
    [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi, 'fill', size(A));

    % plot(X, Y, 'ko', 'MarkerSize', 5, 'MarkerFaceColor', 'k')
    accum_X = [accum_X; X];
    accum_Y = [accum_Y; Y];
  end

  yx_ellipse = [accum_Y, accum_X];
  yx_intersect = intersect(yx_A, yx_ellipse);
  yx_union = union(yx_A, yx_ellipse);
  iou = numel(yx_intersect)/numel(yx_union);
end