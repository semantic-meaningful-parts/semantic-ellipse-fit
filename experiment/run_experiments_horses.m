clear all;
close all;
load('data/pascal_dataset/HORSE_COW');
apply_horse_cow_changes
pascal_horses = horse_training_masks;
pascal_horses_rgb = horse_training_images;
pascal_horses_labels = horse_training_labels;
clear cow*
clear horse*
% Experiments to run
BASE = false;
PRTR = true;
DEFA = false;
AEFA = false;
DEFA = false;
EMAR = false;

% Param-free Ellipse Model
AICBIC_SELECTION = 1; %Set AICBIC_SELECTION = 1, to use AIC is selected else BIC is used
% Our model
ITER = 3;

% Dataset prep
horses = pascal_horses;
horses_rgb = pascal_horses_rgb;
skip = [79, 85, 87, 88, 89, 90, 91, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103];
horses(skip) = [];
horses_rgb(skip) = [];
num_files = numel(horses);
% get = [9, 28, 34, 46, 47, 48, 50, 55, 58, 59];

% Scores
base_iou = zeros(1, num_files);
prtr_iou = zeros(1, num_files);
defa_iou = zeros(1, num_files);
aefa_iou = zeros(1, num_files);
emar_iou = zeros(1, num_files);
% Timers
base_elapsed = zeros(1, num_files);
prtr_elapsed = zeros(1, num_files);
defa_elapsed = zeros(1, num_files);
aefa_elapsed = zeros(1, num_files);
emar_elapsed = zeros(1, num_files);

% Read in image name
for f_id = 1:num_files
  % Input image
  A = horses{f_id};
  % Using our ellipse model with protrusion method
  if PRTR
    prtr_start = tic;
    model = ellipse_model(A, ITER, f_id); % With protrusion
    prtr_elapsed(f_id) = toc(prtr_start);
    % Compute our method error
    prtr_iou(f_id) = ellipse_model_iou(model, A);
  end
  % Using our ellipse model basic
  if BASE
    base_start = tic;
    model = ellipse_model_basic(A, ITER, f_id); % Base global minimization
    base_elapsed(f_id) = toc(base_start);
    % Compute our method error
    base_iou(f_id) = ellipse_model_iou(model, A);
  end
  % Using DEFA method
  if DEFA
    defa_start = tic;
    [~, defa_EL, ~] = runMergeFitting(A,AICBIC_SELECTION); % DEFA method
    close all;
    defa_elapsed(f_id) = toc(defa_start);
    % Compute AEFA error
    defa_iou(f_id) = getEllipseIOU(A, defa_EL);
  end
  % Using AEFA method
  if AEFA
    aefa_start = tic;
    [~ , aefa_EL, ~] = runSlitFitting(A,AICBIC_SELECTION); % AEFA method
    close all;
    aefa_elapsed(f_id) = toc(aefa_start);
    % Compute AEFA error
    aefa_iou(f_id) = getEllipseIOU(A, aefa_EL);
  end
  % Using EMAR method
  if EMAR
    emar_start = tic;
    [~, emar_EL, ~] = runGMMFitting(A,AICBIC_SELECTION); % EMAR method
    close all;
    emar_elapsed(f_id) = toc(emar_start);
    % Compute AEFA error
    emar_iou(f_id) = getEllipseIOU(A, emar_EL);
  end
end

% Compute mean IOU
base_mean_iou = mean(base_iou)
prtr_mean_iou = mean(prtr_iou)
defa_mean_iou = mean(defa_iou)
aefa_mean_iou = mean(aefa_iou)
emar_mean_iou = mean(emar_iou)
% Compute mean runtime per image
base_mean_runtime = mean(base_elapsed)
prtr_mean_runtime = mean(prtr_elapsed)
defa_mean_runtime = mean(defa_elapsed)
aefa_mean_runtime = mean(aefa_elapsed)
emar_mean_runtime = mean(emar_elapsed)




