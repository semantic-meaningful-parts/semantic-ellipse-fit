clear all;
close all;
load('data/mpeg7_dataset/MPEG_7');

% Param-free Ellipse Model
AICBIC_SELECTION = 1; %Set AICBIC_SELECTION = 1, to use AIC is selected else BIC is used
% Our model
ITER = 5;

% Dataset prep
mpeg7 = [mpeg7_bird, mpeg7_bone, mpeg7_camel, mpeg7_deer, mpeg7_dog, ...
         mpeg7_elephant, mpeg7_hammer, mpeg7_horse, mpeg7_rat, ...
         mpeg7_ray, mpeg7_teddy, mpeg7_turtle];
skip = [ 2, 3, 4, 14, 15, 16, 19, 20, 24, 33, 41, 47, 56, 61, 62, 63, 64, 65, 66, 67, 68, ... 
         69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 82, 83, 86, 87, 88, 91, 92, 93, 94, ...
         97, 98, 99, 100, 101, 103, 105, 107, 116, 119, 142, 143, 144, 145, 146, 147, 148, 149, 150, 152, 153, ...
         156, 157, 158, 160, 182, 183, 184, 185, 196, 199, 200, 223, 224, 225, 226, 227, 230, 231, 234 ];
mpeg7(skip) = [];
num_files = numel(mpeg7);

set(0,'DefaultFigureVisible','off');
for f_id = 1:num_files
  A = mpeg7{f_id};

  [IClust,EL,NUMEllipses] = runMergeFitting(A,AICBIC_SELECTION);%DEFA method
  fig = figure; imagesc(scwhiten(A)); 
  title(strcat('MPEG7 DEFA img=', num2str(f_id)));
  hold on;
  for i = 1:numel(EL)
    X0 = EL(i).C(1)-size(A, 2);
    Y0 = EL(i).C(2)-size(A, 1);
    major_axis = EL(i).a;
    minor_axis = EL(i).b;
    phi = EL(i).phi*pi/180;
    [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);

    plot(X, Y, 'ko', 'MarkerSize', 5, 'MarkerFaceColor', 'k')
  end
  print(fig, strcat('samples/mpeg7_defa_', num2str(f_id)),'-dpng')

  [IClust,EL,NUMEllipses] = runSlitFitting(A,AICBIC_SELECTION);%AEFA method
  fig = figure; imagesc(scwhiten(A)); 
  title(strcat('MPEG7 AEFA img=', num2str(f_id)));
  hold on;
  for i = 1:numel(EL)
    X0 = EL(i).C(1)-size(A, 2);
    Y0 = EL(i).C(2)-size(A, 1);
    major_axis = EL(i).a;
    minor_axis = EL(i).b;
    phi = EL(i).phi*pi/180;
    [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);

    plot(X, Y, 'ko', 'MarkerSize', 5, 'MarkerFaceColor', 'k')
  end
  print(fig, strcat('samples/mpeg7_aefa_', num2str(f_id)),'-dpng')

  [IClust,EL,NUMEllipses] = runGMMFitting(A,AICBIC_SELECTION); %EMAR method 
  fig = figure; imagesc(scwhiten(A)); 
  title(strcat('MPEG7 EMAR img=', num2str(f_id)));
  hold on;
  for i = 1:numel(EL)
    X0 = EL(i).C(1)-size(A, 2);
    Y0 = EL(i).C(2)-size(A, 1);
    major_axis = EL(i).a;
    minor_axis = EL(i).b;
    phi = EL(i).phi*pi/180;
    [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);

    plot(X, Y, 'ko', 'MarkerSize', 5, 'MarkerFaceColor', 'k');
  end
  print(fig, strcat('samples/mpeg7_emar_', num2str(f_id)),'-dpng')
end
