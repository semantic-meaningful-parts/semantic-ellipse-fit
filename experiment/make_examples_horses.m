clear all;
close all;
load('data/pascal_dataset/HORSE_COW');
apply_horse_cow_changes
pascal_horses = horse_training_masks;
pascal_horses_rgb = horse_training_images;
pascal_horses_labels = horse_training_labels;
clear cow*
clear horse*

% Param-free Ellipse Model
AICBIC_SELECTION = 1; %Set AICBIC_SELECTION = 1, to use AIC is selected else BIC is used
% Our model
ITER = 5;

% Dataset prep
horses = pascal_horses;
horses_rgb = pascal_horses_rgb;
skip = [79, 85, 87, 88, 89, 90, 91, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103];
horses(skip) = [];
horses_rgb(skip) = [];
num_files = numel(horses);

set(0,'DefaultFigureVisible','off');
for f_id = 1:num_files
  A = horses{f_id};

  [IClust,EL,NUMEllipses] = runMergeFitting(A,AICBIC_SELECTION);%DEFA method
  fig = figure; imagesc(scwhiten(A)); 
  title(strcat('HORSES DEFA img=', num2str(f_id)));
  hold on;
  for i = 1:numel(EL)
    X0 = EL(i).C(1)-size(A, 2);
    Y0 = EL(i).C(2)-size(A, 1);
    major_axis = EL(i).a;
    minor_axis = EL(i).b;
    phi = EL(i).phi*pi/180;
    [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);

    plot(X, Y, 'ko', 'MarkerSize', 5, 'MarkerFaceColor', 'k')
  end
  print(fig, strcat('samples/horses_defa_', num2str(f_id)),'-dpng')

  [IClust,EL,NUMEllipses] = runSlitFitting(A,AICBIC_SELECTION);%AEFA method
  fig = figure; imagesc(scwhiten(A)); 
  title(strcat('HORSES AEFA img=', num2str(f_id)));
  hold on;
  for i = 1:numel(EL)
    X0 = EL(i).C(1)-size(A, 2);
    Y0 = EL(i).C(2)-size(A, 1);
    major_axis = EL(i).a;
    minor_axis = EL(i).b;
    phi = EL(i).phi*pi/180;
    [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);

    plot(X, Y, 'ko', 'MarkerSize', 5, 'MarkerFaceColor', 'k')
  end
  print(fig, strcat('samples/horses_aefa_', num2str(f_id)),'-dpng')

  [IClust,EL,NUMEllipses] = runGMMFitting(A,AICBIC_SELECTION); %EMAR method 
  fig = figure; imagesc(scwhiten(A)); 
  title(strcat('HORSES EMAR img=', num2str(f_id)));
  hold on;
  for i = 1:numel(EL)
    X0 = EL(i).C(1)-size(A, 2);
    Y0 = EL(i).C(2)-size(A, 1);
    major_axis = EL(i).a;
    minor_axis = EL(i).b;
    phi = EL(i).phi*pi/180;
    [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);

    plot(X, Y, 'ko', 'MarkerSize', 5, 'MarkerFaceColor', 'k');
  end
  print(fig, strcat('samples/horses_emar_', num2str(f_id)),'-dpng')
end
