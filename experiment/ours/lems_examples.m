% LEMS ROBOT
ITER = 3;
num_robot_images = numel(lems_robot);
robot_skip = [10, 17, 22, 32, 48, 51, 52, 59, 62, 65, 67, 69, 71];

for index = 1:num_robot_images
  if ismember(index, robot_skip)
    continue;
  end
  ellipse_model(lems_robot{index}, ITER);
end

num_toddler_images = numel(lems_toddler);
toddler_skip = [3, 5, 6, 16 , 31, 41];
for index = 1:num_toddler_images
  if ismember(index, toddler_skip)
    continue;
  end
  ellipse_model(lems_toddler{index}, ITER);
end

num_fish_images = numel(lems_fish);
fish_skip = [10, 11, 12, 14, 16, 18, 33, 36, 50, 62, 72];
for index = 1:num_fish_images
  if ismember(index, fish_skip)
    continue;
  end
  ellipse_model(lems_fish{index}, ITER);
end

num_turtle_images = numel(lems_turtle);
turtle_skip = [];
for index = 1:num_turtle_images
  if ismember(index, turtle_skip)
    continue;
  end
  ellipse_model(lems_turtle{index}, ITER);
end
