clear all;
close all;
load('data/lems_dataset/LEMS');

% Param-free Ellipse Model
AICBIC_SELECTION = 1; %Set AICBIC_SELECTION = 1, to use AIC is selected else BIC is used
% Our model
ITER = 5;
f_id = 99;

% Dataset prep
lems = [lems_fish, lems_robot, lems_toddler, lems_turtle];
skip = [11, 12, 14, 16, 23, 24, 33, 36, 50, 117, 132, 138, 151, 152, 162, 165, 167, 169, 171, 183, 185, 196, 221];
lems(skip) = [];
num_files = numel(lems);
set(0,'DefaultFigureVisible','off');
for f_id = 131:num_files
  A = lems{f_id};

  [IClust,EL,NUMEllipses] = runMergeFitting(A,AICBIC_SELECTION);%DEFA method
  fig = figure; imagesc(scwhiten(A)); 
  title(strcat('LEMS DEFA img=', num2str(f_id)));
  hold on;
  for i = 1:numel(EL)
    X0 = EL(i).C(1)-size(A, 2);
    Y0 = EL(i).C(2)-size(A, 1);
    major_axis = EL(i).a;
    minor_axis = EL(i).b;
    phi = EL(i).phi*pi/180;
    [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);

    plot(X, Y, 'ko', 'MarkerSize', 5, 'MarkerFaceColor', 'k')
  end
  print(fig, strcat('samples/lems_defa_', num2str(f_id)),'-dpng')

  [IClust,EL,NUMEllipses] = runSlitFitting(A,AICBIC_SELECTION);%AEFA method
  fig = figure; imagesc(scwhiten(A)); 
  title(strcat('LEMS AEFA img=', num2str(f_id)));
  hold on;
  for i = 1:numel(EL)
    X0 = EL(i).C(1)-size(A, 2);
    Y0 = EL(i).C(2)-size(A, 1);
    major_axis = EL(i).a;
    minor_axis = EL(i).b;
    phi = EL(i).phi*pi/180;
    [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);

    plot(X, Y, 'ko', 'MarkerSize', 5, 'MarkerFaceColor', 'k')
  end
  print(fig, strcat('samples/lems_aefa_', num2str(f_id)),'-dpng')

  [IClust,EL,NUMEllipses] = runGMMFitting(A,AICBIC_SELECTION); %EMAR method 
  fig = figure; imagesc(scwhiten(A)); 
  title(strcat('LEMS EMAR img=', num2str(f_id)));
  hold on;
  for i = 1:numel(EL)
    X0 = EL(i).C(1)-size(A, 2);
    Y0 = EL(i).C(2)-size(A, 1);
    major_axis = EL(i).a;
    minor_axis = EL(i).b;
    phi = EL(i).phi*pi/180;
    [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);

    plot(X, Y, 'ko', 'MarkerSize', 5, 'MarkerFaceColor', 'k');
  end
  print(fig, strcat('samples/lems_emar_', num2str(f_id)),'-dpng')
end
