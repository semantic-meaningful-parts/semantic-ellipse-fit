clear all;
close all;
load('data/sisha_dataset/SISHA');

% Experiments to run
BASE = true;
PRTR = true;
DEFA = false;
AEFA = false;
DEFA = false;
EMAR = false;

% Param-free Ellipse Model
AICBIC_SELECTION = 1; %Set AICBIC_SELECTION = 1, to use AIC is selected else BIC is used
% Our model
ITER = 3;

% Dataset prep
sisha = sisha_all;
skip = [2, 3, 4, 14, 15, 16, 19, 20, 24, 33, 41, 47, 56, 61, 62, 63, 64, 65, 66, 67, 68, ...
        69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 82, 83, 86, 87, 88, 91, 92, 93, 94, ...
        97, 98, 99, 100, 101, 103, 105, 107, 116, 119, 142, 143, 144, 145, 146, 147, 148, 149, 150, 152, 153, ...
        156, 157, 158, 160, 182, 183, 184, 185, 196, 199, 200, 223, 224, 225, 226, 227, 230, 231, 234];
sisha(skip) = [];
num_files = numel(sisha);

% Scores
base_iou = zeros(1, num_files);
prtr_iou = zeros(1, num_files);
defa_iou = zeros(1, num_files);
aefa_iou = zeros(1, num_files);
emar_iou = zeros(1, num_files);
% Timers
base_elapsed = zeros(1, num_files);
prtr_elapsed = zeros(1, num_files);
defa_elapsed = zeros(1, num_files);
aefa_elapsed = zeros(1, num_files);
emar_elapsed = zeros(1, num_files);

% Read in image name
for f_id = 1:num_files
  % Input image
  A = sisha{f_id};
  % Using our ellipse model with protrusion method
  if PRTR
    prtr_start = tic;
    model = ellipse_model(A, ITER, f_id); % With protrusion
    prtr_elapsed(f_id) = toc(prtr_start);
    % Compute our method error
    prtr_iou(f_id) = ellipse_model_iou(model, A);
  end
  % Using our ellipse model basic
  if BASE
    base_start = tic;
    model = ellipse_model_basic(A, ITER, f_id); % Base global minimization
    base_elapsed(f_id) = toc(base_start);
    % Compute our method error
    base_iou(f_id) = ellipse_model_iou(model, A);
  end
  % Using DEFA method
  if DEFA
    defa_start = tic;
    [~, defa_EL, ~] = runMergeFitting(A,AICBIC_SELECTION); % DEFA method
    close all;
    defa_elapsed(f_id) = toc(defa_start);
    % Compute AEFA error
    defa_iou(f_id) = getEllipseIOU(A, defa_EL);
  end
  % Using AEFA method
  if AEFA
    aefa_start = tic;
    [~ , aefa_EL, ~] = runSlitFitting(A,AICBIC_SELECTION); % AEFA method
    close all;
    aefa_elapsed(f_id) = toc(aefa_start);
    % Compute AEFA error
    aefa_iou(f_id) = getEllipseIOU(A, aefa_EL);
  end
  % Using EMAR method
  if EMAR
    emar_start = tic;
    [~, emar_EL, ~] = runGMMFitting(A,AICBIC_SELECTION); % EMAR method
    close all;
    emar_elapsed(f_id) = toc(emar_start);
    % Compute AEFA error
    emar_iou(f_id) = getEllipseIOU(A, emar_EL);
  end
end

skip

% Compute mean IOU
base_mean_iou = mean(base_iou)
prtr_mean_iou = mean(prtr_iou)
defa_mean_iou = mean(defa_iou)
aefa_mean_iou = mean(aefa_iou)
emar_mean_iou = mean(emar_iou)
% Compute mean runtime per image
base_mean_runtime = mean(base_elapsed)
prtr_mean_runtime = mean(prtr_elapsed)
defa_mean_runtime = mean(defa_elapsed)
aefa_mean_runtime = mean(aefa_elapsed)
emar_mean_runtime = mean(emar_elapsed)




