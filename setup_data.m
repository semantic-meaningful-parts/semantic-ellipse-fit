% Horse and Cow dataset
% fprintf('Loading HORSES and COWS dataset...\n');
% load('data/pascal_dataset/HORSE_COW');
% apply_horse_cow_changes

% fprintf('Loading Weizmann Horses dataset...\n');
% load('data/weizmann_dataset/weizmann_images_db');

fprintf('Loading LEMS dataset... \n');
load('data/lems_dataset/LEMS')

% fprintf('Loading MPEG-7 dataset... \n');
% load('data/mpeg7_dataset/MPEG_7')

% fprintf('Loading SISHA dataset... \n');

% Clean up workspace
clear cow_testing_images cow_training_images_bw  horse_testing_images_bw_seg
clear cow_testing_images_bw cow_training_images_bw_seg horse_testing_images_seg
clear cow_testing_images_bw_seg cow_training_images_seg horse_testing_labels
clear cow_testing_images_seg cow_training_labels horse_testing_masks
clear cow_testing_labels cow_training_masks
clear cow_testing_masks horse_testing_images horse_testing_images_bw
clear cow_training_images
