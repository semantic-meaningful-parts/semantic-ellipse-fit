clear all;
GRAY_PATH = 'data/weizmann_horse_db/gray/';
RGB_PATH = 'data/weizmann_horse_db/rgb/';
load('weizmann_images_labels.mat');
% Get the RGB images names
rgb_image_files = dir(RGB_PATH);
num_rgb = numel(rgb_image_files);
images_retained = [];
for rgb_id = 1:num_rgb
  if ~strcmp(rgb_image_files(rgb_id).name(1), '.')
    images_retained = [images_retained, rgb_id];
  end
end
rgb_image_files = rgb_image_files(images_retained);
num_rgb = numel(rgb_image_files);
weizmann_images = cell(1, num_rgb);
% Populate the RGB images
for rgb_id = 1:num_rgb
  name = rgb_image_files(rgb_id).name;
  image_path = fullfile(RGB_PATH, name);
  img = imread(image_path);
  weizmann_images{rgb_id} = img;
end
% Get the grayscale images
gray_image_files = dir(RGB_PATH);
num_gray = numel(gray_image_files);
images_retained = [];
for gray_id = 1:num_gray
  if ~strcmp(gray_image_files(gray_id).name(1), '.')
    images_retained = [images_retained, gray_id];
  end
end
gray_image_files = gray_image_files(images_retained);
num_gray = numel(gray_image_files);
weizmann_images_bw = cell(1, num_gray);
% Populate the RGB images
for gray_id = 1:num_gray
  name = gray_image_files(gray_id).name;
  image_path = fullfile(GRAY_PATH, name);
  img = imread(image_path);
  weizmann_images_bw{gray_id} = img;
end

num_labels = numel(weizmann_images_labels);
weizmann_images_masks = cell(1, num_labels);
for label_id = 1:num_labels
  img = weizmann_images_labels{label_id};
  img = special_labels(img, 'animals');
  weizmann_images_labels{label_id} = img;
  img(img > 0) = 1;
  img(img < 1) = 0;
  weizmann_images_masks{label_id} = uint8(mat2gray(img));
end

weizmann_images = weizmann_images(1:num_labels);
weizmann_images_bw = weizmann_images_bw(1:num_labels);

VISUAL = false;
if VISUAL
  for img_id = 1:num_labels
    figure;
    subplot(2, 2, 1); imagesc(weizmann_images{img_id});
    subplot(2, 2, 2); imagesc(weizmann_images_bw{img_id});
    subplot(2, 2, 3); imagesc(weizmann_images_masks{img_id});
    subplot(2, 2, 4); imagesc(weizmann_images_labels{img_id});
  end
end

clear GRAY_PATH RGB_PATH VISUAL
clear num_rgb num_gray images_retained num_labels
clear gray_image_files rgb_image_files 
clear img name image_path name rgb_id gray_id label_id


