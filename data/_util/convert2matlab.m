function dataset = convert2matlab(dir_path, opt)
  % Default type parameter
  if ~exist('opt', 'var') || isempty(opt)
    opt = 'default';
  end

  file_names = dir(dir_path);
  num_files = numel(file_names);
  
  % Remove '.'' '..'' '.DS_STORE' and hidden
  idx_ret = [];
  for file_idx = 1:num_files
    if ~strcmp(file_names(file_idx).name(1), '.')
      idx_ret = [idx_ret, file_idx];
    end
  end
  file_names = file_names(idx_ret);
  
  % Import images and convert to MATLAB cell array
  num_files = numel(file_names);
  dataset = cell(1, num_files);
  for file_idx = 1:num_files
    % Generate file path
    file_name = file_names(file_idx).name;
    file_path = strcat(dir_path, file_name);
    % Read and process the image
    img = imread(file_path);
    if strcmp(opt, 'mask')
      img(img > 0) = 1;
      img(img < 1) = 0;
    end
    dataset{file_idx} = img;
  end
end