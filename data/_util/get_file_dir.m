function file_dir = get_file_dir(file_dir)
  % Remove '.'' '..'' '.DS_STORE' and hidden
  idx_ret = [];
  for file_idx = 1:numel(file_dir)
    if ~strcmp(file_dir(file_idx).name(1), '.')
      idx_ret = [idx_ret, file_idx];
    end
  end
  file_dir = file_dir(idx_ret);
end