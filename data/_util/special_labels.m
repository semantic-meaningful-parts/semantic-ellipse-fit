function label_mask = special_labels(label_mask, opt)
  if strcmp(opt, 'animals')
    label_mask(label_mask == 255) = 0; % UNKNOWN
    label_mask(label_mask == 100) = 0; % SILHOUETTE
    label_mask(label_mask > 100 & label_mask < 120) = 1; % HEAD
    label_mask(label_mask == 120) = 2; % TORSO
    label_mask(label_mask == 130) = 3; % NECK
    label_mask(label_mask >= 140 & label_mask < 150) = 4; % LEFT FRONT LEG
    label_mask(label_mask >= 150 & label_mask < 160) = 5; % RIGHT FRONT LEG
    label_mask(label_mask >= 160 & label_mask < 170) = 6; % LEFT BACK LEG
    label_mask(label_mask >= 170 & label_mask < 180) = 7; % RIGHT BACK LEG
    label_mask(label_mask == 180) = 8; % TAIL
  end
end