clear all;

load('data/pascal_dataset/HORSE_COW');
load('data/pascal_dataset/fixed_horse_cow_training_masks/fixed_horse_training_masks.mat');
load('data/pascal_dataset/fixed_horse_cow_training_masks/gt_horse_labels.mat');
fixed_horse_training_labels = gt;
VISUAL = false;
num_training = numel(horse_training_images);
fixed_horse_training_images_seg = cell(1, num_training);
if VISUAL
  figure;
end
% Applying horse fixes
for training_id = 1:num_training
  if training_id <= numel(fixed_horse_training_labels)
    fixed_horse_training_label = special_labels(fixed_horse_training_labels{training_id}, ...
                                                'animals');
    horse_training_labels{training_id} = fixed_horse_training_label;
    fixed_horse_training_mask = fixed_horse_training_label;
    fixed_horse_training_mask(fixed_horse_training_mask > 0) = 1;
  else
    fixed_horse_training_mask = fixed_horse_training_masks{training_id};
  end
  fixed_horse_training_mask = uint8(mat2gray(fixed_horse_training_mask));
  fixed_horse_training_mask(fixed_horse_training_mask > 0) = 1;
  fixed_horse_training_masks{training_id} = fixed_horse_training_mask;
  horse_training_image = horse_training_images{training_id};
  fixed_horse_training_image_seg = horse_training_image.*repmat(fixed_horse_training_mask, [1, 1, 3]);
  fixed_horse_training_images_seg{training_id} = fixed_horse_training_image_seg;

  horse_training_image_bw = horse_training_images_bw{training_id};
  fixed_horse_training_image_bw_seg = horse_training_image_bw.*fixed_horse_training_mask;
  fixed_horse_training_images_bw_seg{training_id} = fixed_horse_training_image_bw_seg;
  if VISUAL
    subplot(2, 4, 1); imagesc(horse_training_images{training_id});
    subplot(2, 4, 5); imagesc(horse_training_images_bw{training_id});
    subplot(2, 4, 2); imagesc(horse_training_masks{training_id});
    subplot(2, 4, 6); imagesc(fixed_horse_training_masks{training_id});
    subplot(2, 4, 3); imagesc(horse_training_images_seg{training_id});
    subplot(2, 4, 4); imagesc(horse_training_images_bw_seg{training_id});
    subplot(2, 4, 7); imagesc(fixed_horse_training_images_seg{training_id});
    subplot(2, 4, 8); imagesc(fixed_horse_training_images_bw_seg{training_id});

    suptitle(['Visualization for Test Image #', num2str(training_id)]);
    pause;
  end % end-if-VISUAL
end % end-for-training_id

% Store changes
horse_training_masks = fixed_horse_training_masks;
horse_training_images_seg = fixed_horse_training_images_seg;
horse_training_images_bw_seg = fixed_horse_training_images_bw_seg;

for_test = ...
[1, 2, 3, 4, 5, 6, 7, 13, 14, 16, ...
17, 18, 20, 21, 22, 24, 26, 27, ...
28, 31, 32, 35, 38, 39, 41, 42, ...
43, 44, 46, 47, 48, 49, 52, 53, ...
55, 56, 57, 58, 59, 60, 61, 62, ...
64, 65, 66, 67, 69, 70, 71, 72, ...
74, 75, 77, 79, 80, 82, 83, 86, ...
88, 89, 90, 91, 92, 93, 94, 95, ...
96, 98, 99, 100, 101, 103, 104, ...
105, 106, 107, 108, 109, 110, 111, ...
112, 113, 114, 115, 116, 117, 118, ...
119, 120, 121, 122, 123, 124, 130, ...
131, 132, 133, 134, 135, 136, 138, ...
139, 140];

horse_training_masks = horse_training_masks(for_test);
horse_training_labels = horse_training_labels(for_test);
horse_training_images_seg = horse_training_images_seg(for_test);
horse_training_images_bw = horse_training_images_bw(for_test);
horse_training_images_bw_seg = horse_training_images_bw_seg(for_test);
horse_training_images = horse_training_images(for_test);

% Clean up environment;
clear num_training VISUAL training_id for_test gt;
clear horse_training_image;
clear horse_training_image_bw;
clear fixed_horse_training_mask;
clear fixed_horse_training_masks;
clear fixed_horse_training_label;
clear fixed_horse_training_labels;
clear fixed_horse_training_image_seg;
clear fixed_horse_training_images_seg;
clear fixed_horse_training_image_bw_seg;
clear fixed_horse_training_images_bw_seg;