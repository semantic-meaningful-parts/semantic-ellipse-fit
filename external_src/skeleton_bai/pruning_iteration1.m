function [skel_image, endpoint, branches, tendpoint] = pruning_iteration1(bw, I0, endpoint, branches, threshold)
%----------------------------------------------------------------------- 
%Desc: iterative  algorithm  to  prune  the  skeleton. 
% Para:bw denotes skeleton map
%      In bw, Non-zero point present the radius of its maximal disk
%      I0 the object image, m �� n matrix ,Non-zero point present the
%      position of the shape
%      endpoint, branches, threshold its' own name present itself
%Return: skel_image,the skeleton image
%      endpoint, branches its' own name present itself  
%      tendpoint is a temp to endpoint
%----------------------------------------------------------------------- 
len = length(branches);
[tm, tn] = size(bw);
sum_im = zeros(tm, tn);
for i = 1:len
    im_path_re{i,1} = path_reconstruct(bw, branches{i,1});
    sum_im = sum_im + im_path_re{i,1};
end
weight_r = 0;

%compute the center point
[px, py] = find(bw ~= 0);
[mx, my] = find(bw == max(max(bw)));
dis = sqrt((mx(1) - px).^2 + (my(1)  - py).^2);
l = find(dis == min(dis));
center = [px(l(1)), py(l(1))];

run = 1;
while (weight_r < threshold) && run
   [tendpoint] = endpoint;
  [endpoint, branches, im_path_re, sum_im, weight_r, remove_endpoint, remove_branch,run]=remove_one(endpoint, branches, im_path_re, sum_im,bw,center,run);
end

temp2 = ones(tm, tn);
[tm1, tn1] = size(endpoint);
for i = 1:tm1
    [temp1] = branches{i ,1};
    [tm2, tn2] = size(temp1);
    for j = 1:tm2
        temp2(temp1(j, 1), temp1(j, 2)) = 0;
    end
end

[temp1] = remove_branch;
[tm2, tn2] = size(temp1);
for j = 1:tm2
    temp2(temp1(j, 1), temp1(j, 2)) = 0;
end

skel_image = temp2+I0;



function  [endpoint, branches, im_path_re, sum_im, weight_r, remove_endpoint, remove_branch,run] = remove_one(endpoint, branches, im_path_re, sum_im,bw,center,run)
%----------------------------------------------------------------------- 
%Desc: remove one end branch with the lowest weight 
%----------------------------------------------------------------------
len = length(branches);
[bwm,bwn] = size(bw);
tlength = length(find(sum_im~=0));
for i = 1:len
    temp = sum_im - im_path_re{i,1};
    weight(i) = 1-length(find(temp~=0))/tlength;
end
[l, s] = sort(weight);
weight_r = l(1);
no = s(1);

tendpoint = endpoint;
tbranches = branches;
tim_path_re = im_path_re;
tsum_im = sum_im;
remove_endpoint = endpoint(no,:);
remove_branch = branches{no,1};
sum_im = sum_im - im_path_re{no,1};

if no == 1
   [endpoint] = [endpoint(2:len,:)];
   [branches] = {branches{2:len,1}}';
   [im_path_re] = {im_path_re{2:len,1}}';
   flag = 1;
elseif no == len
   [endpoint] = [endpoint(1:len-1,:)];
   [branches] = {branches{1:len-1,1}}';
   [im_path_re] = {im_path_re{1:len-1,1}}';
   flag = 2;
else
   [endpoint] = [endpoint(1:no-1,:);endpoint(no+1:len, :)];
   [branches] = {branches{1:no-1,1},branches{no+1:len,1}}';
   [im_path_re] = {im_path_re{1:no-1,1},im_path_re{no+1:len,1}}';
   flag = 3;
end

%compute numbers of main branches from center point
[path_num] = main_path(branches);
if path_num == 1
    [junctionPoints] = findNearJunction([bwm,bwn],branches,center); 
    if (isempty(junctionPoints))
        [endpoint,branches,im_path_re,sum_im] = all_recovery(tendpoint,tbranches,tim_path_re,tsum_im);
        run = 0;   
    elseif (~isempty(junctionPoints))
       
        center = junctionPoints(1,:);
        [endpoint,sum_im] = endpoint_recovery(tendpoint,tsum_im);
        [branches] = GetNewBranchPath(bw, center, endpoint);
        len = length(branches);
        for i = 1:len
            im_path_re{i,1} = path_reconstruct(bw, branches{i,1});
        end
          
    end
end

function  [endpoint,sum_im] = endpoint_recovery(tendpoint,tsum_im)
%recover the endpoint and sum_im 
endpoint = tendpoint;
sum_im = tsum_im;

function [endpoint,branches,im_path_re,sum_im] = all_recovery(tendpoint,tbranches,tim_path_re,tsum_im)
%recover endpoint,branches,im_path_re,sum_im
endpoint = tendpoint;
branches = tbranches;
im_path_re = tim_path_re;
sum_im = tsum_im;

function [branches] = GetNewBranchPath(bw, center, endpoint)
%recompute branches path again
[tm, tn] = size(endpoint);
for i = 1 : tm;  
    branches{i,1} = pathDFS1(bw, center, endpoint(i,:));
end

function [path_num] = main_path(branches)
%compute numbers of main branches from center point
branchtemp = branches{1,1};
center_path_point(1,:) = branchtemp(10,:);
for i = 2:length(branches)
     branchtemp = branches{i,1};
     tr = 0;
     [mm,nn] = size(center_path_point);
     for j = 1:mm
        
         if branchtemp(10,:) == center_path_point(j,:)
            tr = 1;
            break;
         end
        
     end
     if tr == 0
        [center_path_point] = [center_path_point;branchtemp(10,:)];
     end    
end
[mm,nn] = size(center_path_point);
path_num = mm;