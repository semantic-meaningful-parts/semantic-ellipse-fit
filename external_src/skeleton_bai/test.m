bw = '6.GIF';
I = imread(bw);
threshold = 0.03;

[skel_image, skel_dist, I0, endpoint, branches, tendpoint] = DSE(I,50, threshold);
%

bw1 = strcat(bw ,'-skeleton.jpg');
imwrite(skel_image/2,bw1);