function [junctionPoints] = findNearJunction(daxiao,branches,center)
%----------------------------------------------------------------------- 
%Desc: find junctions near the center point     
%Para: daxiao ,the size of skeleton map
%      branches is branches remained
%      center is the center point
%Return: junctionPoints is junctionpoints near the center point
%----------------------------------------------------------------------- 
temp = zeros(daxiao(1,1), daxiao(1,2));

for i = 1:length(branches)
     branchtemp = branches{i,1};
     [m, n] = size(branchtemp);
     for j = 1:m
         temp(branchtemp(j,1),branchtemp(j,2)) = temp(branchtemp(j,1),branchtemp(j,2)) + 1;
     end
end

    endPoints = [];
    junctionPoints = [];
for i = 1:length(branches)
    branchtemp = branches{i,1};
    [m, n] = size(branchtemp);
    this_endpoint = temp(branchtemp(m,1),branchtemp(m,2));

    for j = 1:(m)
       if temp(branchtemp((m+1-j),1),branchtemp((m+1-j),2)) ~= this_endpoint
            if temp(branchtemp((m+1-j),1),branchtemp((m+1-j),2)) == center
                [endPoints] = [endPoints;this_endpoint];   %    [A] = [A;4 15 14 1]
                break;
            else
                [junctionPoints] = [junctionPoints;branchtemp((m+1-j),1),branchtemp((m+1-j),2)];
                break;
            end
       end
    end
end


    new_junctionPoints = [];
for i = 1:length(branches)
    branchtemp = branches{i,1};
    [m, n] = size(branchtemp);
    flag = 0;
    for j = 1:m
        [tm, tn] = size(junctionPoints);
            for k = 1:tm
                if branchtemp(j,:) == junctionPoints(k,:)
                    ding = 1;
                    [lm, ln] = size(new_junctionPoints);
                    tflag = 1;
                    for l = 1:lm
                        if junctionPoints(k,:) == new_junctionPoints(l,:)
                            tflag = 0;
                            break;
                        end 
                     
                    end
                            if tflag && ding
                                [new_junctionPoints] = [new_junctionPoints;junctionPoints(k,:)];
                                flag = 1;
                                break;
                            end

                end
            end
                       if flag 
                        break;
                       end
    end
end
  junctionPoints = new_junctionPoints;                        