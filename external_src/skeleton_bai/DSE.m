function [skel_image, skel_dist, I0, endpoint, branches, tendpoint] = DSE(bw, n_0, threshold)
%skel_image:output skeleton
%skel_dist: distance transform
%I0:binary mask
%bw: input binary mask
%n_0: the number of vertices for the simified polygon by DCE, usually a constant between 30~50
%threshold: the stop threshold for DSE
%--------------------------------------------------------------------------------------------
%This version can not solve the binary shape with holes, which will be given in the forthcoming version.
%The authors of this software are Xiang Bai & Yao Xu, Huazhong Univ. of Science and Tech., Wuhan, China.
%Contact Email: {xiang.bai, quickxu}@gmail.com



[m,n]=size(bw);
bw = imresize(bw,350/max(m,n));


bw1=preprocess(bw);
I=1-bw1;
%%to show the original image
%figure;
%imshow(I,[]);

[bw,I0,x,y,x1,y1]=div_skeleton_new(4,1,1,I,n_0);
skel_dist = bw;
I0= double(I0);
    
    [branches] = GetBranchPath(bw, I0, [x1,y1]);

endpoint = [x1,y1];

% threshold = 0.0005;%bone
%threshold = 0.005;

% [endpoint, branches, weight_r] = pruning_iteration(bw, I0, endpoint, branches, threshold);

[skel_image, endpoint, branches, tendpoint] = pruning_iteration1(bw, I0, endpoint, branches, threshold);