function [ra_vertices,po_vertices]=All_pairradius_DFS(bw,num,x1,y1);
len=length(x1);
ra_vertices=zeros(num,1);
po_vertices=zeros(num,1);
for i=1:len;
    [radius,position]=Comput_pairradius_DFS(bw,i,num,x1,y1);
    ra_vertices=[ra_vertices,radius];
    po_vertices=[po_vertices,position];
end
[m,n]=size(ra_vertices);
ra_vertices=[ra_vertices(:,2:n)];
[m,n]=size(po_vertices);
po_vertices=[po_vertices(:,2:n)];


function [radius,position]=Comput_pairradius_DFS(bw,k,num,x1,y1);


radius=zeros(num,1);
position=zeros(num,1);
len=length(x1);
for i=k+1:len;  
    s_p=[x1(k),y1(k)];
    e_p=[x1(i),y1(i)];
    [r,sa]=Radius_sequence_DFS(bw,s_p,e_p,num);
    radius=[radius,r];
    position=[position,sa];
%[start_p, end_p]=nearest_skel_p(bw,s_p,e_p);
%[S,R,Y]=dijkstra_bai1(bw,bw,start_p,end_p);
%[sa,r]=sam_skel_path(bw,Y,num);
end

for i=1:k-1;
    if k>1;
        s_p=[x1(k),y1(k)];
        e_p=[x1(i),y1(i)];
        [r,sa]=Radius_sequence_DFS(bw,s_p,e_p,num); 
        radius=[radius,r];
        position=[position,sa];
    end
end

radius=[radius(:,2:len)];
position=[position(:,2:2*len-1)];

function [r,sa]=Radius_sequence_DFS(bw,s_p,e_p,num);
% [start_p, end_p]=nearest_skel_p(bw,s_p,e_p);
% [S,R,Y]=dijkstra_bai1(bw,bw,start_p,end_p);
[P,S]=pathDFS(bw,s_p,e_p);
% figure;
% imshow(S);
% hold on;
% plot(P(:,2),P(:,1),'.r');
[sa,r]=sam_skel_path(bw,P,num);

