fprintf('Starting up environment...\n');
fprintf('Suppressing Name Conflict warning...\n');
warning('off', 'MATLAB:dispatcher:nameConflict');

% Adding external_src
fprintf('Adding skeletonization toolbox setup...\n');
addpath(genpath('external_src/skeleton_telea'));
fprintf('Adding fit ellipse toolbox...\n');
addpath(genpath('external_src/fit_ellipse'))
fprintf('Running VLFeat toolbox setup...\n');
addpath(genpath('external_src/vlfeat'));
fprintf('Adding SC toolbox...\n')
addpath(genpath('external_src/sc'));
fprintf('Adding Fast Marching v3b toolbox...\n');
addpath(genpath('external_src/FastMarching_version3b'));

% Add source code after in case fxn names overlap with external code
fprintf('Adding path for source code and data...\n');
addpath(genpath('src'));
addpath(genpath('src/_util'));
addpath(genpath('data'));
addpath(genpath('data/lems_dataset'));
addpath(genpath('data/mpeg7_dataset'));
addpath(genpath('data/sisha_dataset'));
addpath(genpath('data/weizmann_dataset'));
addpath(genpath('data/pascal_dataset'));
addpath(genpath('data/_util'));
addpath(genpath('experiment'));
addpath(genpath('experiment/ours'));
addpath(genpath('experiment/param_free_ellipse'))

% Set up the datasets
setup_data;
