% get_params
%
% @param funct name of function
% @param params the set of params for specified for the function
%
% Given a function to look up, we return the set of internal
% parameters needed for the function

function params = get_params(funct)
  digits_dataset = false;
  % Constants
  PATCH_SIGMA = 4;
  PATCH_CONV_SIZE = 3;
  JOINT_SIGMA = 8;
  JOINT_CONV_SIZE = 14;
  EDGE_COLORS = ['y', 'm', 'c', 'r', 'g', 'b', 'w'];
  % Parameter structure
  params = struct('y', 1, ...
                  'x', 2, ...
                  'first', 1, ...
                  'VISUAL', false, ...
                  'VOCAL', false);
  
  SEGMENT_STRUCT = struct('master', [], ... % Master branch of the segment
                          'm_endpoints', [], ... % End points of the branch
                          'peripherals', {}, ... % Periperal branches attached to master
                          'p_endpoints', {}, ... % End points of peripherals
                          'is_terminal', false, ...
                          'contour', [], ...
                          'contour_plot', [], ...
                          'patch', []);

  PATCH_STRUCT = struct('part', [], ...
                        'patch', [], ...
                        'patch_weights', [], ...
                        'junctions', [], ... 
                        'relative_part', [], ... % relative coordinates of part (centered at center of patch)
                        'relative_junctions', [], ...
                        'part_mask', [], ...
                        'centroid', []);            

  % Define ellipse struct
  ELLIPSE_STRUCT = struct('a', [],...
                          'b', [],...
                          'phi', [],...
                          'X0', [],...
                          'Y0', [],...
                          'X0_in', [],...
                          'Y0_in', [],...
                          'long_axis', [],...
                          'short_axis', [],...
                          'status', '', ...
                          'shape', []);

  if strcmp(funct, 'default')
    return;
  %%
  % Feature extraction functions
  %%
  elseif strcmp(funct, 'meaningful_parts')
    params.VOCAL = false;
    params.VISUAL = false;
    params.OUTLIER_MULT = 1.5;
    params.PARTS2PATCHES_OPT = 'full';
    params.PATCH_STRUCT = PATCH_STRUCT;
    params.SEGMENT_STRUCT = SEGMENT_STRUCT;
    params.ELLIPSE_STRUCT = ELLIPSE_STRUCT;
  elseif strcmp(funct, 'decomp_by_gradient')
    params.VOCAL = false;
    params.VISUAL = false;
    params.SAMPLE_SIZE = 6; 
    params.START_POS = params.SAMPLE_SIZE;
    params.MIN_SIZE = (params.SAMPLE_SIZE*2)-1;
    params.WINDOW_SPAN = 5; 
    params.CUT_THRESHOLD = 135;
  elseif strcmp(funct, 'compose_by_no_contour')
    params.SYMM_AXIS_SAMPLE_SIZE = 100;
    params.NEIGHBOR_RADIUS = 3;
    params.STEP_SIZE = 6;
    params.THETA_THRESHOLD = 135;
    params.WINDOW_SPAN = 10;
  elseif strcmp(funct, 'compose_by_protrusion')
    params.SAMPLE_SIZE = 10;
    params.NEIGHBOR_RADIUS = 3;
    params.STEP_SIZE = 1;
    % FOR PASCAL HORSES
    params.STRENGTH_THRESHOLD = 0.48; % Protrudes just as much as the base
    % FOR LEMS 
    params.STRENGTH_THRESHOLD = 0.7;
  elseif strcmp(funct, 'compose_by_gradient')
    params.SYMM_AXIS_AFFINITY_THRESH = 125;
    params.CONTOUR_AFFINITY_THRESH = 125;
    params.MIN_PROBABILITY = 0.50;
    params.SIZE_PERCENT = 0.25;
    params.SYMM_AXIS_SAMPLE_SIZE = 100;
    params.CONTOUR_SAMPLE_SIZE = 20;
    params.MAX_SIZE_FOR_MERGE = 80; 
    params.NEIGHBOR_RADIUS = 3;
    params.STEP_SIZE = 6;
    params.THETA_THRESHOLD = 135;
    params.WINDOW_SPAN = 10;
  elseif strcmp(funct, 'conf_by_protrusion')
    params.SAMPLE_SIZE = 10;
    params.NEIGHBOR_RADIUS = 3;
    params.STEP_SIZE = 1;
  elseif strcmp(funct, 'compose2parts')
    params.SYMM_AXIS_AFFINITY_THRESH = 90;
    params.CONTOUR_AFFINITY_THRESH = 135;
    params.MIN_SIZE_ALLOWED = 6;
    if digits_dataset
      params.SYMM_AXIS_SAMPLE_SIZE = 5;
      params.CONTOUR_SAMPLE_SIZE = 5;
      params.MAX_SIZE_FOR_MERGE = 10;
    else
      params.SYMM_AXIS_SAMPLE_SIZE = 100;
      params.CONTOUR_SAMPLE_SIZE = 20;
      params.MAX_SIZE_FOR_MERGE = 80;  
    end
  elseif strcmp(funct, 'parts2patches')
    CONV_SIZE = [4, 4];
    SIGMA = 4;
    params.VISUAL = false;
    params.G = fspecial('gaussian', CONV_SIZE, SIGMA);
    if digits_dataset
      params.BUFFER = 2;
    else
      params.BUFFER = 0;
    end
    params.FALLOFF_PART = 0.2;
    params.FALLOFF_JOINT = 0.1; % I think this one should be smaller (yield's larger spread) than FALLOFF_PART
  elseif strcmp(funct, 'ellipse_model')
    params.ELLIPSE_STRUCT = ELLIPSE_STRUCT;
    params.NEIGHBOR_RADIUS = 3;
    params.STEP_SIZE = 6;
    params.WINDOW_SIZE = 16;
  %%
  % Training procedure functions
  %%
  elseif strcmp(funct, 'train_model')
    params.ELLIPSE_STRUCT = ELLIPSE_STRUCT;
  %%
  % Utility functions
  %%
  elseif strcmp(funct, 'utility')
  %%
  % Solver functions
  %%

  %%
  % Experiments
  %%
  elseif strcmp(funct, 'run_experiment')
    params.EDGE_COLORS = EDGE_COLORS;
    params.VISUAL = true;
  end
end
