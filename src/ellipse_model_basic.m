function models = ellipse_model(mask, MAX_ITER, img_id)
  % Get internal parameters 
  params = get_params('ellipse_model');
  params.VISUAL = true;
  dims = size(mask);

  % Get the indices for the shape image
  yx_mask = get_labels(mask);
  yx_mask = yx_mask{1};
  % Get the major symmetry axis of this shape
  tmp_mask = zeros(dims);
  tmp_mask(yx2index([1, 1; dims], yx_mask)) = 1;
  mask = single_region(tmp_mask);

  % Total time start
  time_start = tic;

  % Compute symmetry axis of the shape mask
  sa_start = tic;
  skeleton = symmetry_axis(mask, 'default');
  sa_elapsed = toc(sa_start);

  % Decompose the symmetry axis to segments, this is our initial grouping
  d2s_start = tic;
  segments = decompose2segments(skeleton);
  d2s_elapsed = toc(d2s_start);
  
  % Populate the segment struct and sort them by outside to in
  pss_start = tic;
  segment_struct = populate_segment_struct(segments, mask);
  segment_struct = sort_segment_struct(segment_struct, 'out_in');
  pss_elapsed = toc(pss_start);

  % Compose the segments that are in fact a part of another segment 
  cs_start = tic;
  %segment_struct = compose_by_singly(segment_struct, mask);
  cs_elapsed = toc(cs_start);

  shapes = {segment_struct.patch};
  shapes = plot_pixels(shapes, mask);
  segments = {segment_struct.master};

  [local_models, ~] = fit_ellipse_model(shapes, MAX_ITER);

  local_models = cell2mat(local_models);
  num_models = numel(local_models);
  yx_ellipse_fill = cell(1, num_models);
  for ellipse_id = 1:num_models
    X0 = local_models(ellipse_id).X0_in;
    Y0 = local_models(ellipse_id).Y0_in;
    major_axis = local_models(ellipse_id).a;
    minor_axis = local_models(ellipse_id).b;
    phi = local_models(ellipse_id).phi;
    [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi, 'fill', dims);
    yx_ellipse_fill{ellipse_id} = [Y, X];
  end

  [assign, ~, ~, ~, empty] = assign2parts_msfm(yx_ellipse_fill, mask);
  local_models(empty) = [];
  unassign = [];
  for a_id = 1:numel(assign)
    if isempty(assign{a_id})
      unassign = [unassign, a_id];
    end
  end
  assign(unassign) = [];
  local_models(unassign) = [];

  shapes = plot_pixels(assign, mask);
  models = fit_ellipse_model_global(shapes, local_models, MAX_ITER);

  if false
    [sp, sa] = plot_segment_struct(segment_struct, mask, 'overlay');
    fig = figure; 
    subplot(1, 2, 1); imagesc(scwhiten(sa)); title(strcat('Local Basic img=', num2str(img_id)));
    hold on;
    for ellipse_id = 1:numel(local_models)
      X0 = local_models(ellipse_id).X0_in;
      Y0 = local_models(ellipse_id).Y0_in;
      major_axis = local_models(ellipse_id).a;
      minor_axis = local_models(ellipse_id).b;
      phi = local_models(ellipse_id).phi;
      % Try plotting the ellipses
      [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);
      % Visualization
      plot(X, Y, 'ko', 'MarkerSize', 5, 'MarkerFaceColor', 'k')
    end
    subplot(1, 2, 2); imagesc(scwhiten(sa)); title(strcat('Global Basic img=', num2str(img_id)));
    hold on;
    for ellipse_id = 1:numel(models)
      X0 = models(ellipse_id).X0_in;
      Y0 = models(ellipse_id).Y0_in;
      major_axis = models(ellipse_id).a;
      minor_axis = models(ellipse_id).b;
      phi = models(ellipse_id).phi;
      % Try plotting the ellipses
      [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);
      % Visualization
      plot(X, Y, 'ko', 'MarkerSize', 5, 'MarkerFaceColor', 'k')
    end
    print(fig, strcat('samples/basic_', num2str(img_id)),'-dpng')
  end
end



function yx_plot = single_region(yx_plot)
  stats = regionprops(logical(yx_plot), 'Area', 'PixelIdxList');
  % If there are multiple regions (labeling errors)
  if numel(stats) > 1
    areas = cell2mat({stats.Area});
    pixel_list = {stats.PixelIdxList};
    max_region = find(areas == max(areas));
    for r_id = 1:numel(stats)
      if r_id == max_region
        continue;
      end
      yx_plot(pixel_list{r_id}) = 0;
    end
    [y, x] = find(yx_plot == 1);
    yx = [y, x];
    yx_plot = zeros(size(yx_plot));
    yx_plot(yx2index([1, 1,; size(yx_plot)], yx)) = 1;
  end
end % end-function