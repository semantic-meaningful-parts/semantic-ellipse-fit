% meaningful_patches
%
% @param dataset dataset containing images
% @param sample_id sample index within the dataset
% @param class_id class label for the particular sample
% @return patch_info struct containing the meaningful parts extracted
% from the image
%
% Given an image from a dataset, apply symmetry axis transform and 
% decompose the axis into a set of parts by junctions. Extract the image
% patches corresponding to the parts. We denote these patches as 
% meaningful patches.

function conf_plot = meaningful_parts(img, mask)
  % Set up internal parameters
  params = get_params('meaningful_parts');
  % If we are not given the mask
  if ~exist('mask', 'var') || isempty(mask)
    mask = img;
    % Set up image to prune out weaker pixels
    mask_arr = reshape(mask, 1, []);
    std_dev = std(double(mask_arr));
    max_value = max(mask_arr);
    strength_lim = max_value-(params.OUTLIER_MULT*std_dev);
    % Remove weak pixels
    mask(mask < strength_lim) = 0;
    % Generate our own mask
    mask(mask > 0) = 1;
  end % end-if-!exist
  % Total time start
  time_start = tic;
  % Get symmetry axis
  sa_start = tic;
  skeleton = symmetry_axis(mask, 'default');
  sa_elapsed = toc(sa_start);
  % Decompose the symmetry axis to segments, this is our initial grouping
  d2s_start = tic;
  segments = decompose2segments(skeleton);
  d2s_elapsed = toc(d2s_start);

  % place code here brian
  TESTBTAY = 0;
  if TESTBTAY; 
    segments_0 = segments;
    segment_struct_0 = populate_segment_struct(segments_0, mask);
    [sp_plot_0, sa_plot_0] = plot_segment_struct(segment_struct_0, mask, 'overlay');
  end

  if TESTBTAY;
    segments = decomp_by_edge_dist(segments, mask);
    [sp_plot, sa_plot] = plot_segment_struct(segment_struct, mask, 'overlay');
  end
  % Populate the segment struct and sort them by outside to in
  pss_start = tic;
  segment_struct = populate_segment_struct(segments, mask);
  segment_struct = sort_segment_struct(segment_struct, 'out_in');
  pss_elapsed = toc(pss_start);

  %%%
  % Composition steps for candidates
  %%%

  % Compose the segments without a contour associated
  cnc_start = tic;
  segment_struct = compose_by_no_contour(segment_struct);
  cnc_elapsed = toc(cnc_start);
  [sp_plot_cnc, sa_plot_cnc] = plot_segment_struct(segment_struct, mask, 'overlay');
  % Compose the segments by the amount of protrusion
  cp_start = tic;
  segment_struct = compose_by_protrusion(segment_struct, mask);
  cp_elapsed = toc(cp_start);
  [sp_plot_cp, sa_plot_cp] = plot_segment_struct(segment_struct, mask, 'overlay');

  if TESTBTAY;
    [sp_plot_2, sa_plot_2] = plot_segment_struct(segment_struct, mask, 'overlay');
    fig(100); imagesc([sp_plot_0, sp_plot, sp_plot_2]); 
    title('parts og | parts edge dist decomp | parts composed');
    fig(101); imagesc([sa_plot_0, sa_plot, sa_plot_2]);
    title('axes og | axes edge dist decomp | axes composed');
    keyboard;
  end
  % Compose the segments by gradient direction of contour and segment
  cg_start = tic;
  segment_struct = compose_by_gradient(segment_struct, mask);
  cg_elapsed = toc(cg_start);
  [sp_plot_cg, sa_plot_cg] = plot_segment_struct(segment_struct, mask, 'overlay');

  %%%
  % Fine tuning step
  %%%

  % Get high confidence pixels, split at protrusion base
  conf_start = tic;
  [patch_assign, protru_bases, conf_plot] = conf_by_protrusion(segment_struct, mask);
  patches = {segment_struct.patch};
  high_conf_plot = high_conf_assign(patches, patch_assign, mask, 'show_low_conf');
  conf_elapsed = toc(conf_start);
  % Run a Graph Cut pixel assignment give 
  gc_start = tic;
  opts = struct; Q = load('modelFinal'); opts.edge_model = Q.model;
  E = edgesDetect(img, Q.model);
  ed = 0.75 * (1.0 - E);
  % run final graph cuts part
  opts.E = E;
  opts.patch_mask = high_conf_assign(patches, patch_assign, mask);
  opts.conf_plot = conf_plot;
  opts.high_conf_plot = high_conf_plot;
  sp_plot_gc = assign2parts_gc(segment_struct, mask, img, opts);
  gc_elapsed = toc(gc_start);
  % Assign get total time elapsed
  time_elapsed = toc(time_start);
  % Visualize the compositions
  if params.VISUAL
    if iambtay(); fig(2); else; figure; end;
    % figure; imagesc(conf_plot);
    % Plot original image
    subplot(3, 4, 1); imagesc(img); title('Original image');
    % Plot final with base of protrusion
    subplot(3, 4, 2); imagesc(sp_plot_cg); title('Results after Compositions'); 
    hold on;
    yx_protru_bases = cell2mat(protru_bases(:));
    for i = 1:size(yx_protru_bases, 1)
      plot(yx_protru_bases(i, params.x), yx_protru_bases(i, params.y), 'ko', 'MarkerSize', 10);
    end
    hold off
    % Plot final with after deciding on high confidence pixels
    subplot(3, 4, 3); imagesc(high_conf_plot); title('High confidence assignments');
    % Plot final with graph cut pixel assignment
    subplot(3, 4, 4); imagesc(sp_plot_gc); title('Graph cuts assignments');
    % Plot initial mask and symmetry axes
    subplot(3, 4, 5); imagesc(mask);
    subplot(3, 4, 9); imagesc(skeleton);
    % Plot results after applying compose by no contour
    subplot(3, 4, 6); imagesc(sp_plot_cnc); title('Semantic Parts after Compose by No Contour');
    subplot(3, 4, 10); imagesc(sa_plot_cnc); title('Symmetry Axes after Compose by No Contour');
    % Plot results after applying compose by protrusion
    subplot(3, 4, 7); imagesc(sp_plot_cp); title('Semantic Parts after Compose by Protrusion');
    subplot(3, 4, 11); imagesc(sa_plot_cp); title('Symmetry Axes after Compose by Protrusion');
    % Plot results after applying compose by gradient
    subplot(3, 4, 8); imagesc(sp_plot_cg); title('Semantic Parts after Compose by Gradient');
    subplot(3, 4, 12); imagesc(sa_plot_cg); title('Symmetry Axes after Compose by Gradient');
  end

  if params.VOCAL
    fprintf('\n');
    fprintf('Elapsed time for extracting symmetry axis: %.3f seconds\n', ...
      sa_elapsed);
    fprintf('Elapsed time for decomposing to segments: %.3f seconds\n', ...
      d2s_elapsed);
    fprintf('Elapsed time for populating and sorting segment structures: %.3f seconds\n', ...
      pss_elapsed);
    fprintf('Elapsed time for composing segments by no contour: %.3f seconds\n', ...
      cnc_elapsed);
    fprintf('Elapsed time for composing segments by protrusion: %.3f seconds\n', ...
      cp_elapsed);
    fprintf('Elapsed time for composing segments by gradient: %.3f seconds\n', ...
      cg_elapsed);
    fprintf('Elapsed time for finding high confidence pixels by protrusion: %.3f seconds\n', ...
      conf_elapsed);
    fprintf('Elapsed time for finding graph cuts pixel assignment: %.3f seconds\n', ...
      gc_elapsed);
    fprintf('Total time incurred: %.3f seconds\n', ...
      time_elapsed); 
  end
  patch_struct = 0;
end % end-function

function high_conf_plot = high_conf_assign(patches, patch_assign, mask, opt)
  if ~exist('opt', 'var') || isempty(opt)
    opt = 'default';
  end
  high_conf_plot = zeros(size(mask));
  num_patches = numel(patches);
  if strcmp(opt, 'show_low_conf')
    high_conf_plot = mask;
    high_conf_plot(high_conf_plot ~= 0) = num_patches+1;
  end
  % Plot the points 
  for p_id = 1:num_patches
    assignment = patch_assign{p_id};
    retained = find(assignment == 1);
    patch = patches{p_id};
    patch = patch(retained, :);
    patch_ind = sub2ind(size(mask), patch(:, 1), patch(:, 2));
    high_conf_plot(patch_ind) = p_id;
  end
end
