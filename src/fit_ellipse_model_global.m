function models = fit_ellipse_model_global(shapes, local_models, MAX_ITER)

  num_models = numel(local_models);

  dims = size(shapes);
  yx_shapes = get_labels(shapes);
  VISUAL = false;
  DEBUG = true;
  % Set up merged shaped
  is_merged = false(1, num_models);
  start_iter = true;
  models = local_models;
  while start_iter | min(d_cost_mat) < 0

    num_models = numel(models);
    if num_models < 3
      break;
    end
    start_iter = false;
    candidates = cell(num_models, num_models);
    d_cost_mat = nan(num_models, num_models);
    adj_graph = to_adj_graph(shapes, 3, 'masks');
    o_cost = ellipse_model_iou(models, shapes);

    for p_id = 1:num_models
      % Get the first shape we are going to consider
      pyx_shape = yx_shapes{p_id}; % yx-shape
      %local_model = local_models(p_id);
      adj_list = adj_graph(p_id, :);
      % Get the adjacency list for shape-p
      adj_sparse = find(adj_list == 1);
      num_adj = numel(adj_sparse);
      % Let's consider each adjacent shape
      for a_id = 1:num_adj
        q_id = adj_sparse(a_id);

        qyx_shape = yx_shapes{q_id};
        uyx_shapes = [pyx_shape; qyx_shape];
        % Set up visualizations for ellipse fitting
        if VISUAL
          figure; imagesc(plot_pixels({uyx_shapes}, zeros(dims)));
          hold on;
        end % end-if-params.VISUAL

        out_of_bounds = false;
        % Fit ellipse via least-squares
        ellipse_struct = fit_ellipse(uyx_shapes(:, 2), uyx_shapes(:, 1));
        if ~isempty(ellipse_struct.X0)
          X0 = ellipse_struct.X0_in;
          Y0 = ellipse_struct.Y0_in;
          major_axis = ellipse_struct.a;
          minor_axis = ellipse_struct.b;
          phi = ellipse_struct.phi;
          % Try plotting the ellipses
          [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);
          % Visualization
          if VISUAL
            plot(X, Y, 'ko', 'MarkerSize', 3)
          end % end-if-params.VISUAL
          if (isempty(X0) || isempty(Y0)) || ...
             (isempty(X) || isempty(Y)) || ...
             (max(Y) > dims(1) || max(X) > dims(2))
            % Set flag that out of bounds
            out_of_bounds = true;
            fprintf('WARNING: Points on ellipse out of bounds.\n');
            fprintf('(X0, Y0)=(%i, %i), X=(%i, %i), Y=(%i, %i), max(Y)=%d, max(X)=%d\n', ...
              X0, Y0, ...
              size(X, 1), size(X, 2), ...
              size(Y, 1), size(Y, 2), ...
              max(Y), max(X));
          end
        end % end-if-~isempty(ellipse_struct.X0)

        % Calculate cost for each ellipse fit
        if ~out_of_bounds && ~isempty(ellipse_struct.X0)
          %cost = eval_cost(ellipse_struct, uyx_shapes, dims);
          %local_model = [local_models{p_id}, local_models{q_id}];
          %o_cost = eval_cost(local_model, uyx_shapes, dims);
          %d_cost_mat(p_id, q_id) = cost-o_cost; 
          candidate_model = models;
          candidate_model(q_id) = [];
          candidate_model(p_id) = ellipse_struct;
          cost = ellipse_model_iou(candidate_model, shapes);
          d_cost_mat(p_id, q_id) = cost-o_cost;
          candidates{p_id, q_id} = ellipse_struct;
        else 
          d_cost_mat(p_id, q_id) = inf;
        end % end-if-out-of-bounds
      end % end-for-a_id
    end % end-for-p_id
    % Find the greedily minimized cost
    [p_id, q_id] = find(d_cost_mat == min(d_cost_mat(:)));
    p_id = p_id(1); q_id = q_id(1);
    yx_shapes{p_id} = [yx_shapes{p_id}; yx_shapes{q_id}];
    yx_shapes(q_id) = [];
    shapes = plot_pixels(yx_shapes, shapes);
    models(p_id) = candidates{p_id, q_id};
    %is_merged(q_id) = true;
    models(q_id) = [];
  end % while-min(d_cost_mat) < 0
  %models = local_models(find(~is_merged));
end

% Deprecated
function cost = eval_cost(ellipse_struct, yx_shape, dims)
  num_ellipses = numel(ellipse_struct);
  ellipse_plot = zeros(dims);
  fit_cost = 0;
  accum_X = []; accum_Y = [];
  % Plot the areas of the ellipse
  for e_id = 1:num_ellipses
    X0 = ellipse_struct(e_id).X0_in;
    Y0 = ellipse_struct(e_id).Y0_in;
    major_axis = ellipse_struct(e_id).a;
    minor_axis = ellipse_struct(e_id).b;
    phi = ellipse_struct(e_id).phi;
    if ~isempty(X0) && ~isempty(Y0)
      [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi, 'fill', dims);
    end
    % Failure to fit ellipse
    if (isempty(X0) || isempty(Y0)) || ...
       (isempty(X) || isempty(Y)) || ...
       (max(Y) > dims(1) || max(X) > dims(2))
      fit_cost = inf;
      fprintf('WARNING: Failed to fit ellipse onto region.\n');
      fprintf('X0=(%i, %i), Y0=(%i, %i), X=(%i, %i), Y=(%i, %i), max(Y)=%d, max(X)=%d\n', ...
              size(X0, 1), size(X0, 2), ...
              size(Y0, 1), size(Y0, 2), ...
              size(X, 1), size(X, 2), ...
              size(Y, 1), size(Y, 2), ...
              max(Y), max(X))
      break;
    end
    accum_X = [accum_X; X];
    accum_Y = [accum_Y; Y];
  end % end-for-e_id
  yx_ellipse = [accum_Y, accum_X];
  yx_intersect = intersect(yx_ellipse, yx_shape);
  yx_union = union(yx_ellipse, yx_shape);
  cost = 1-(numel(yx_intersect)/numel(yx_union));
end % end-function