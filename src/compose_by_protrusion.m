function segment_struct = compose_by_protrusion(segment_struct, mask)
  MAX_RADIUS = max(size(mask));
  params = get_params('compose_by_protrusion');
  params.DEBUG = false;
  params.VISUAL = false;
  num_segments = numel(segment_struct);
  is_merged = false(1, num_segments);
  % Need the segments and their junctions to measure protrusion
  segment_masters = {segment_struct.master};
  segment_m_endpoints = {segment_struct.m_endpoints};
  segment_peripherals = {segment_struct.peripherals};
  segment_p_endpoints = {segment_struct.p_endpoints};
  segment_is_terminal = {segment_struct.is_terminal};
  segment_contours = {segment_struct.contour};
  segment_contour_plots = {segment_struct.contour_plot};
  segment_patches = {segment_struct.patch};
  segment_terminals = find([segment_struct.is_terminal] == true);
  % Find complex junctions by locating those with multiple segments
  segment_sets = cell(1, num_segments);
  % Join the segment masters and peripherals together
  for s_id = 1:num_segments
    segment_sets{s_id} = [segment_masters{s_id}, segment_peripherals{s_id}];
  end
  % Concatenate all segment contours to produce full contour
  full_contour = [];
  for c_id = 1:num_segments
    full_contour = [full_contour; segment_contours{c_id}];
  end

  % Find the set of junctions connecting the segments
  junctions = find_junctions(segment_sets, size(mask), 3);
  % Iterate through the complex junctions to find the segments it connects

  if params.DEBUG
    disp_segments = cell(1, numel(segment_sets));
    for disp_seg = 1:numel(segment_sets)
      disp_segment = segment_sets{disp_seg};
      disp_segment = disp_segment{1};
      disp_segments{disp_seg} = disp_segment;
    end
    figure; imagesc(plot_pixels(disp_segments, mask, 'overlay'))
    hold on;
  end
  keys = junctions.keys;
  num_keys = numel(keys); 
  for key_id = 1:num_keys
    key = keys{key_id};
    yx_junction = index2yx([1, 1; size(mask)], key);
    % Get the segments connected to this junction
    values = junctions(key);
    num_values = numel(values);
    if isempty(intersect(values, segment_terminals))
      continue;
    end
    % Protrusion strength matrix
    strength_mat = nan(1, num_values);
    for value_id = 1:num_values
      value = values(value_id);
      % We only want to do this for the set of terminal segments
      if ~ismember(value, segment_terminals)
        continue;
      end
      % Get vector AB by finding the segment in the master branch joined to yx_junction
      master_segments = segment_masters{value};
      [vector_AB, segment_AB]= segment_vector(yx_junction, master_segments, params.SAMPLE_SIZE);
      A = vector_AB(params.first, :);
      % Find the closest point on the contour to get the radius
      yx_contour = segment_contours{value};
      % Build a kd-tree containing all the points in the mask contour
      kd_tree = vl_kdtreebuild(yx_contour');
      try
      [~, dist] = vl_kdtreequery(kd_tree, yx_contour', yx_junction');
      catch e; display(e); keyboard; end;
      l_points = []; r_points = [];
      epsilon = 0; full_search = false;
      % Search until we have found a pair of points on both the left and right
      while isempty(l_points) || isempty(r_points)
        radius = sqrt(dist)+epsilon;
        [index, ~] = rangesearch(yx_contour, yx_junction, radius);
        edge_points = yx_contour(cell2mat(index), :);
        % Iterate through the edge points
        num_edge_points = size(edge_points, 1);
        signs_mat = zeros(1, num_edge_points);
        theta_mat = zeros(1, num_edge_points);
        for point_id = 1:num_edge_points
          P = edge_points(point_id, :);
          % Generate the vector AP
          vector_AP = [A ; P];
          % Compute the sign of the determinant of vectors
          signs_mat(point_id) = sign_det_vectors(vector_AB, vector_AP);
          % Generate the vector PB, from the junction to a given point P
          vector_PB = [P; yx_junction];
          % Meausure the inner angle between our segment and the point P
          theta_mat(point_id) = inner_angle(vector_AB, vector_PB);
        end % end-for-point_id
        invalid_points = find(theta_mat > 90);
        signs_mat(invalid_points) = nan;
        % Separate the points to the left or right of the segment
        l_points = find(signs_mat == -1);
        r_points = find(signs_mat == 1);
        epsilon = epsilon+1;
        if epsilon >= MAX_RADIUS
          fprintf('WARNING: Search range exceeds image size using partial contour, but no points found\n');
          fprintf('DEBUG: segment_id=%i\n', values(:));
          if ~full_search
            epsilon = 0; full_search = true;
            yx_contour = full_contour;
            % Rebuild KD-Tree
            kd_tree = vl_kdtreebuild(yx_contour');
            fprintf('Retrying with full contour search...\n');
          else
            fprintf('WARNING: Search range exceeds image size using full contour, but no points found\n');
            break;
          end
        end
      end % end-while-(isempty(l_points) || isempty(r_points))
      num_left = numel(l_points);
      num_right = numel(r_points);
      if num_left == 0 || num_right == 0
        continue;
      end 
      convexity_mat = nan(num_left, num_right);
      % Find the convexity of the left and right points
      for l_id = 1:num_left
        l_point = edge_points(l_points(l_id), :);
        vector_L = [l_point; yx_junction];
        for r_id = 1:num_right
          r_point = edge_points(r_points(r_id), :);
          vector_R = [r_point; yx_junction];
          % Compute the inner angle as the convexity between two points
          convexity_mat(l_id, r_id) = inner_angle(vector_L, vector_R);
        end % end-for-r_id
      end % end-for-l_id
      % Get the most convex or the minimal inner angle
      [l, r] = find(convexity_mat == max(convexity_mat(:)));
      
      if params.DEBUG
        plot_l = edge_points(l_points(l), :);
        plot_r = edge_points(r_points(r), :);
        plot(plot_l(2), plot_l(1), 'ko', 'markerSize', 10) 
        plot(plot_r(2), plot_r(1), 'ko', 'markerSize', 10) 
      end
      
      % Establish the base of the protrusion
      l_base = edge_points(l_points(l(params.first)), :);
      l_dist = pdist([l_base; yx_junction], 'euclidean');
      r_base = edge_points(r_points(r(params.first)), :);
      r_dist = pdist([r_base; yx_junction], 'euclidean');
      radius = max([l_dist, r_dist]);
      dist_base = pdist([l_base; r_base], 'euclidean');
      [peak, dist_peak] = find_peak(yx_junction, segment_AB, yx_contour, params.SAMPLE_SIZE);
      dist_protru = dist_peak-radius;
      if isempty(peak)
        strength_mat(value_id) = nan;
      else
        strength_mat(value_id) = dist_protru/dist_base;
      end
    end % end-for-value_id
    %values
    %strength_mat
    % If a region protrusion strength is above threshold, should be own part
    above_thresh_ids = find(strength_mat >= params.STRENGTH_THRESHOLD);
    num_above = numel(above_thresh_ids);
    % If a region protrusion strength is below threshold
    below_thresh_ids = find(strength_mat < params.STRENGTH_THRESHOLD);
    num_below = numel(below_thresh_ids);
    % Then it should be merged to the part above threshold with sharpest angle
    for below_id = 1:num_below
      below_thresh_id = below_thresh_ids(below_id);
      b_segment_id = values(below_thresh_id);
      b_master_segments = segment_masters{b_segment_id};
      b_endpoints = segment_m_endpoints{b_segment_id};
      b_peripherals = segment_peripherals{b_segment_id};
      b_p_endpoints = segment_p_endpoints{b_segment_id};
      b_contour = segment_contours{b_segment_id};
      b_contour_plot = segment_contour_plots{b_segment_id};
      b_patch = segment_patches{b_segment_id};
      vector_BP = segment_vector(yx_junction, b_master_segments, params.SAMPLE_SIZE);
      rho_mat = nan(1, num_values);
      rho_id = [];
      % If there are regions that sits above threshold
      if num_above > 0
        % Then those below becomes the peripherals of the sharpest angle
        for above_id = 1:num_above
          above_thresh_id = above_thresh_ids(above_id);
          a_segment_id = values(above_thresh_id);
          a_master_segments = segment_masters{a_segment_id};
          vector_AP = segment_vector(yx_junction, a_master_segments, params.SAMPLE_SIZE);
          rho_mat(above_thresh_id) = inner_angle(vector_AP, vector_BP);
        end % end-for-above_id
      else % If there is no part above threshold
        non_term_ids = setdiff(values, segment_terminals);
        num_non_term = numel(non_term_ids);
        % Then merge it with the flattest non-terminal segment
        for non_id = 1:num_non_term
          n_segment_id = non_term_ids(non_id);
          non_term_id = find(values == n_segment_id);
          n_master_segments = segment_masters{n_segment_id};
          vector_NP = segment_vector(yx_junction, n_master_segments, params.SAMPLE_SIZE);
          rho_mat(non_term_id) = inner_angle(vector_NP, vector_BP);
        end % end-for-non_term_id
      end % end-if-num_above > 0
      
      rho_id = find(rho_mat == max(rho_mat(:)));
      if isempty(rho_id)
        continue;
      end 
      select_id = values(rho_id);
      % Merge b_segment and its endpoints into the selected segment above
      segment_peripherals{select_id} = [segment_peripherals{select_id}, b_master_segments, b_peripherals];
      segment_p_endpoints{select_id} = [segment_p_endpoints{select_id}, b_endpoints, b_p_endpoints];
      segment_is_terminal{select_id} = true;
      segment_contours{select_id} = [segment_contours{select_id}; b_contour];
      segment_contour_plots{select_id} = segment_contour_plots{select_id}+b_contour_plot;
      segment_patches{select_id} = [segment_patches{select_id}; b_patch];
      % Mark as merged
      is_merged(b_segment_id) = true;
    end % end-for-below_id
  end % end-for-key_id
  % Update the segment_struct
  for segment_id = 1:num_segments 
    segment_struct(segment_id).peripherals = segment_peripherals{segment_id};
    segment_struct(segment_id).p_endpoints = segment_p_endpoints{segment_id};
    segment_struct(segment_id).is_terminal = segment_is_terminal{segment_id};
    segment_struct(segment_id).contour = segment_contours{segment_id};
    segment_struct(segment_id).contour_plot = segment_contour_plots{segment_id};
    segment_struct(segment_id).patch = segment_patches{segment_id};
  end % end-for-segment_id
  merged_ids = find(is_merged == true);
  retained = setdiff(1:num_segments, merged_ids);
  segment_struct = segment_struct(retained);
end % end-function

