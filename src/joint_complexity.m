function complexities = joint_complexity(segments)
  FRONT = 1; END = 2;
  num_segments = numel(segments);
  complexities = zeros(2, num_segments);
  for curr_id = 1:num_segments
    % Get front and end of current segment
    curr_segment = segments{curr_id};
    curr_front = curr_segment(1, :);
    curr_end = curr_segment(end, :);
    for next_id = 1:num_segments
      % Sanity check
      if curr_id == next_id
        continue;
      end
      % Get front and end of next segment
      next_segment = segments{next_id};
      next_front = next_segment(1, :);
      next_end = next_segment(end, :);
       % Check which ends may be connected and increment the complexity
      if pdist([curr_front; next_front]) < 3
        complexities(FRONT, curr_id) = complexities(FRONT, curr_id)+1;
      elseif pdist([curr_front; next_end]) < 3
        complexities(FRONT, curr_id) = complexities(FRONT, curr_id)+1;
      elseif pdist([curr_end; next_front]) < 3
        complexities(END, curr_id) = complexities(END, curr_id)+1;
      elseif pdist([curr_end; next_end]) < 3
        complexities(END, curr_id) = complexities(END, curr_id)+1;
      end % end-if-isequal(curr_front, next_front)
    end % end-for-curr_id
  end % end-for-next_id
end % end-function