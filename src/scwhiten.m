function o = scwhiten(labels)
o1 = sc(labels, 'jet', [0,8]); % assuming there's a max of 8 parts for now
msk_blue = o1(:,:,1) == 0 & o1(:,:,2) == 0 & o1(:,:,3) == 0.5;
msk_blue3 = cat(3, msk_blue, msk_blue, msk_blue);
o = max(o1, msk_blue3);
%imwrite(o, num2str(clock), 'png');
end