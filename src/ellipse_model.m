function models = ellipse_model(mask, MAX_ITER, img_id)
  % Get internal parameters 
  params = get_params('ellipse_model');
  params.VISUAL = true;
  dims = size(mask);

  % Get the indices for the shape image
  yx_mask = get_labels(mask);
  yx_mask = yx_mask{1};
  % Get the major symmetry axis of this shape
  tmp_mask = zeros(dims);
  tmp_mask(yx2index([1, 1; dims], yx_mask)) = 1;
  mask = single_region(tmp_mask);

  % Total time start
  time_start = tic;

  % Compute symmetry axis of the shape mask
  sa_start = tic;
  skeleton = symmetry_axis(mask, 'default');
  sa_elapsed = toc(sa_start);

  % Decompose the symmetry axis to segments, this is our initial grouping
  d2s_start = tic;
  segments = decompose2segments(skeleton);
  d2s_elapsed = toc(d2s_start);
  
  % Populate the segment struct and sort them by outside to in
  pss_start = tic;
  segment_struct = populate_segment_struct(segments, mask);
  segment_struct = sort_segment_struct(segment_struct, 'out_in');
  pss_elapsed = toc(pss_start);

  cs_start = tic;
  segment_struct = compose_by_singly(segment_struct, mask);
  cs_elapsed = toc(cs_start);

  % Compose the segments without a contour associated
  cnc_start = tic;
  segment_struct = compose_by_no_contour(segment_struct);
  cnc_elapsed = toc(cnc_start);
  [sp_plot_cnc, sa_plot_cnc] = plot_segment_struct(segment_struct, mask, 'overlay');

  % Compose the segments by the amount of protrusion
  cp_start = tic;
  segment_struct = compose_by_protrusion(segment_struct, mask);
  cp_elapsed = toc(cp_start);
  [sp_plot_cp, sa_plot_cp] = plot_segment_struct(segment_struct, mask, 'overlay');

  % Compose the segments by gradient direction of contour and segment
  cg_start = tic;
  segment_struct = compose_by_gradient(segment_struct, mask);
  cg_elapsed = toc(cg_start);
  [sp_plot_cg, sa_plot_cg] = plot_segment_struct(segment_struct, mask, 'overlay');

  % Get high confidence pixels, split at protrusion base
  conf_start = tic;
  [patch_assign, protru_bases, conf_plot] = conf_by_protrusion(segment_struct, mask);
  patches = {segment_struct.patch};
  high_conf_plot = high_conf_assign(patches, patch_assign, mask, 'show_low_conf');
  conf_elapsed = toc(conf_start);

  % Fit ellipses onto the 
  fem_start = tic;
  [local_models, costs] = fit_ellipse_model(conf_plot, 3);
  fem_elapsed = toc(fem_start);

  local_models = cell2mat(local_models);
  num_models = numel(local_models);
  yx_ellipse_fill = cell(1, num_models);
  for ellipse_id = 1:num_models
    X0 = local_models(ellipse_id).X0_in;
    Y0 = local_models(ellipse_id).Y0_in;
    major_axis = local_models(ellipse_id).a;
    minor_axis = local_models(ellipse_id).b;
    phi = local_models(ellipse_id).phi;
    [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi, 'fill', dims);
    yx_ellipse_fill{ellipse_id} = [Y, X];
  end

  [assign, ~, ~, ~, empty] = assign2parts_msfm(yx_ellipse_fill, mask);
  local_models(empty) = [];
  unassign = [];
  for a_id = 1:numel(assign)
    if isempty(assign{a_id})
      unassign = [unassign, a_id];
    end
  end
  assign(unassign) = [];
  local_models(unassign) = [];

  shapes = plot_pixels(assign, mask);

  models = fit_ellipse_model_global(shapes, local_models, 3);
  
  if true
    [sp, sa] = plot_segment_struct(segment_struct, mask, 'overlay');
    set(0,'DefaultFigureVisible','off');
    fig = figure; 
    subplot(1, 2, 1); 
    imagesc(scwhiten(sa)); title(strcat('Local Protrusion img=', num2str(img_id)));
    hold on;
    for ellipse_id = 1:numel(local_models)
      X0 = local_models(ellipse_id).X0_in;
      Y0 = local_models(ellipse_id).Y0_in;
      major_axis = local_models(ellipse_id).a;
      minor_axis = local_models(ellipse_id).b;
      phi = local_models(ellipse_id).phi;
      %Try plotting the ellipses
      [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);
      % Visualization
      plot(X, Y, 'ko', 'MarkerSize', 5, 'MarkerFaceColor', 'k')
    end

    set(0,'DefaultFigureVisible','off');
    subplot(1, 2, 2); 
    imagesc(scwhiten(sa)); title(strcat('Global Protrusion img=', num2str(img_id)));
    hold on;
    for ellipse_id = 1:numel(models)
      X0 = models(ellipse_id).X0_in;
      Y0 = models(ellipse_id).Y0_in;
      major_axis = models(ellipse_id).a;
      minor_axis = models(ellipse_id).b;
      phi = models(ellipse_id).phi;
      % Try plotting the ellipses
      [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);
      % Visualization
      plot(X, Y, 'ko', 'MarkerSize', 5, 'MarkerFaceColor', 'k')
    end
    set(0,'DefaultFigureVisible','on');
    figure;
    imagesc(scwhiten(sa)); title(strcat('Local Protrusion img=', num2str(img_id)));
    hold on;
    for ellipse_id = 1:numel(local_models)
      X0 = local_models(ellipse_id).X0_in;
      Y0 = local_models(ellipse_id).Y0_in;
      major_axis = local_models(ellipse_id).a;
      minor_axis = local_models(ellipse_id).b;
      phi = local_models(ellipse_id).phi;
      % Try plotting the ellipses
      [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);
      % Visualization
      plot(X, Y, 'ko', 'MarkerSize', 5, 'MarkerFaceColor', 'k')
    end

    figure;
    imagesc(scwhiten(sa)); title(strcat('Global Protrusion img=', num2str(img_id)));
    hold on;
    for ellipse_id = 1:numel(models)
      X0 = models(ellipse_id).X0_in;
      Y0 = models(ellipse_id).Y0_in;
      major_axis = models(ellipse_id).a;
      minor_axis = models(ellipse_id).b;
      phi = models(ellipse_id).phi;
      % Try plotting the ellipses
      [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);
      % Visualization
      plot(X, Y, 'ko', 'MarkerSize', 5, 'MarkerFaceColor', 'k')
    end
    print(fig, strcat('samples/protru_', num2str(img_id)),'-dpng')
  end




  % Get total time elapsed
  time_elapsed = toc(time_start);

  % Visualize the compositions
  if false % params.VISUAL
    figure; imagesc(mask);
    figure; imagesc(sa_plot_cnc);
    figure;
    imagesc(conf_plot); 
    hold on;
    yx_protru_bases = cell2mat(protru_bases(:));
    for i = 1:size(yx_protru_bases, 1)
      plot(yx_protru_bases(i, params.x), yx_protru_bases(i, params.y), 'ko', 'MarkerSize', 10);
    end
    hold off

    figure;
    % Plot original image
    subplot(3, 4, 1); imagesc(mask); title('Original image');
    % Plot final with base of protrusion
    subplot(3, 4, 2); imagesc(sp_plot_cg); title('Results after Compositions'); 
    hold on;
    yx_protru_bases = cell2mat(protru_bases(:));
    for i = 1:size(yx_protru_bases, 1)
      plot(yx_protru_bases(i, params.x), yx_protru_bases(i, params.y), 'ko', 'MarkerSize', 10);
    end
    hold off
    % Plot final with after deciding on high confidence pixels
    subplot(3, 4, 3); imagesc(high_conf_plot); title('High confidence assignments');
    % Plot final with after deciding on high confidence pixels
    subplot(3, 4, 4); imagesc(conf_plot); title('High confidence assignments');
    % Plot initial mask and symmetry axes
    subplot(3, 4, 5); imagesc(mask);
    subplot(3, 4, 9); imagesc(skeleton);
    % Plot results after applying compose by no contour
    subplot(3, 4, 6); imagesc(sp_plot_cnc); title('Semantic Parts after Compose by No Contour');
    subplot(3, 4, 10); imagesc(sa_plot_cnc); title('Symmetry Axes after Compose by No Contour');
    % Plot results after applying compose by protrusion
    subplot(3, 4, 7); imagesc(sp_plot_cp); title('Semantic Parts after Compose by Protrusion');
    subplot(3, 4, 11); imagesc(sa_plot_cp); title('Symmetry Axes after Compose by Protrusion');
    % Plot results after applying compose by gradient
    subplot(3, 4, 8); imagesc(sp_plot_cg); title('Semantic Parts after Compose by Gradient');
    subplot(3, 4, 12); imagesc(sa_plot_cg); title('Symmetry Axes after Compose by Gradient');
  end
  % Vocalize time spent in each step
  if params.VOCAL
    fprintf('\n');
    fprintf('Elapsed time for extracting symmetry axis: %.3f seconds\n', ...
      sa_elapsed);
    fprintf('Elapsed time for decomposing to segments: %.3f seconds\n', ...
      d2s_elapsed);
    fprintf('Elapsed time for populating and sorting segment structures: %.3f seconds\n', ...
      pss_elapsed);
    fprintf('Elapsed time for composing segments by no contour: %.3f seconds\n', ...
      cnc_elapsed);
    fprintf('Elapsed time for composing segments by protrusion: %.3f seconds\n', ...
      cp_elapsed);
    fprintf('Elapsed time for composing segments by gradient: %.3f seconds\n', ...
      cg_elapsed);
    fprintf('Elapsed time for finding high confidence pixels by protrusion: %.3f seconds\n', ...
      conf_elapsed);
    fprintf('Elapsed time for fitting ellipse model: %.3f seconds\n', ...
      fem_elapsed);
    fprintf('Total time incurred: %.3f seconds\n', ...
      time_elapsed); 
  end

end % end-function


function yx_plot = single_region(yx_plot)
  stats = regionprops(logical(yx_plot), 'Area', 'PixelIdxList');
  % If there are multiple regions (labeling errors)
  if numel(stats) > 1
    areas = cell2mat({stats.Area});
    pixel_list = {stats.PixelIdxList};
    max_region = find(areas == max(areas));
    for r_id = 1:numel(stats)
      if r_id == max_region
        continue;
      end
      yx_plot(pixel_list{r_id}) = 0;
    end
    [y, x] = find(yx_plot == 1);
    yx = [y, x];
    yx_plot = zeros(size(yx_plot));
    yx_plot(yx2index([1, 1,; size(yx_plot)], yx)) = 1;
  end
end % end-function


function high_conf_plot = high_conf_assign(patches, patch_assign, mask, opt)
  if ~exist('opt', 'var') || isempty(opt)
    opt = 'default';
  end
  high_conf_plot = zeros(size(mask));
  num_patches = numel(patches);
  if strcmp(opt, 'show_low_conf')
    high_conf_plot = mask;
    high_conf_plot(high_conf_plot ~= 0) = num_patches+1;
  end
  % Plot the points 
  for p_id = 1:num_patches
    assignment = patch_assign{p_id};
    retained = find(assignment == 1);
    patch = patches{p_id};
    patch = patch(retained, :);
    patch_ind = sub2ind(size(mask), patch(:, 1), patch(:, 2));
    high_conf_plot(patch_ind) = p_id;
  end
end
