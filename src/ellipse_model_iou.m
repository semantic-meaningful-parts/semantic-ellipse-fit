function iou = ellipse_model_iou(models, mask)
  [y_mask, x_mask] = find(mask == 1);
  yx_mask = [y_mask, x_mask];

  accum_X = []; accum_Y = [];
  for ellipse_id = 1:numel(models)
    X0 = models(ellipse_id).X0_in;
    Y0 = models(ellipse_id).Y0_in;
    major_axis = models(ellipse_id).a;
    minor_axis = models(ellipse_id).b;
    phi = models(ellipse_id).phi;
    [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi, 'fill', size(mask));
    accum_X = [accum_X; X];
    accum_Y = [accum_Y; Y];
  end

  yx_ellipse = [accum_Y, accum_X];
  yx_intersect = intersect(yx_mask, yx_ellipse);
  yx_union = union(yx_mask, yx_ellipse);
  iou = numel(yx_intersect)/numel(yx_union);
end