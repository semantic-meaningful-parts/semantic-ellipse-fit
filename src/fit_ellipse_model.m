function [models, cost_mat] = fit_ellipse_model(mask, MAX_ITER)
  % Get internal parameters 
  params = get_params('ellipse_model');
  params.VISUAL = false;
  dims = size(mask);
  % Get the indices for the shape image
  yx_shapes = get_labels(mask);
  num_shapes = numel(yx_shapes);
  models = cell(1, num_shapes);
  cost_mat = zeros(1, num_shapes);
  for s_id = 1:num_shapes
    yx_shape = yx_shapes{s_id};
    % Get the major symmetry axis of this shape
    tmp_shape = zeros(dims);
    tmp_shape(yx2index([1, 1; dims], yx_shape)) = 1;
    shape = single_region(tmp_shape);
    [y_shape, x_shape] = find(shape == 1);
    yx_shape = [y_shape, x_shape];
    skeleton = symmetry_axis(shape, 'default');
    % Decompose skeleton into segments
    shape_axis = decompose2segments(skeleton);
    % If there are multiple axes
    if numel(shape_axis) > 1
      % We need to find the main branch in symmetry axis
      shape_axis = merge_simple(shape_axis, params.NEIGHBOR_RADIUS);  
      sizes = zeros(1, numel(shape_axis));
      for i = 1:numel(shape_axis)
        sizes(i) = size(shape_axis{i}, 1);
      end
      % Choose the major axis as our selected axis
      select_id = find(sizes == max(sizes));
      shape_axis = shape_axis{select_id};
    else % We have a single axis, so extract it from the cell
      shape_axis = shape_axis{1};
    end
    % First we fit the ellipses to their local parts
    local_cost_mat = zeros(1, MAX_ITER);
    local_candidates = cell(1, MAX_ITER);
    % Try to find the best local fit of ellipses
    for iter = 1:MAX_ITER
      % Perform k-cuts on the shape
      [sub_shapes, sub_axes, yx] = k_cuts(shape_axis, ...
                                          yx_shape, ...
                                          iter-1, ... % 1st iteration should be 0-cut
                                          params.STEP_SIZE, ...
                                          params.WINDOW_SIZE);

      % We could not find anymore locations to cut
      if isempty(sub_axes) 
        local_cost_mat(iter:MAX_ITER) = inf;
        break;
      end
      % Initialize ellipse struct storage
      num_sub_shapes = numel(sub_shapes);
      ellipse_struct = repmat(params.ELLIPSE_STRUCT, 1, num_sub_shapes);
      % Set up visualizations for ellipse fitting
      if false % params.VISUAL
        figure; imagesc(plot_pixels(yx_shapes, zeros(dims)));
        hold on;
      end % end-if-params.VISUAL
      % Fit ellipse on each of the shapes
      out_of_bounds = false;
      fail_fitting = [];
      for shape_id = 1:num_sub_shapes
        shape_i = sub_shapes{shape_id};
        % Fit ellipse via least-squares
        ellipse = fit_ellipse(shape_i(:, 2), shape_i(:, 1));
        if isempty(ellipse) || isempty(ellipse.X0)
          fail_fitting = [fail_fitting, shape_id];
          continue;
        end
        ellipse_struct(shape_id) = ellipse;
        if ~isempty(ellipse_struct(shape_id).X0)
          X0 = ellipse_struct(shape_id).X0_in;
          Y0 = ellipse_struct(shape_id).Y0_in;
          major_axis = ellipse_struct(shape_id).a;
          minor_axis = ellipse_struct(shape_id).b;
          phi = ellipse_struct(shape_id).phi;
          % Try plotting the ellipses
          [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);
          % Visualization
          if false % params.VISUAL
            plot(X, Y, 'ko', 'MarkerSize', 5, 'MarkerFaceColor', 'k')
          end % end-if-params.VISUAL
          if (isempty(X0) || isempty(Y0)) || ...
             (isempty(X) || isempty(Y)) || ...
             (max(Y) > dims(1) || max(X) > dims(2))
            % Set flag that out of bounds
            out_of_bounds = true;
            fprintf('WARNING: Points on ellipse out of bounds.\n');
            fprintf('(X0, Y0)=(%i, %i), X=(%i, %i), Y=(%i, %i), max(Y)=%d, max(X)=%d\n', ...
              X0, Y0, ...
              size(X, 1), size(X, 2), ...
              size(Y, 1), size(Y, 2), ...
              max(Y), max(X));
          end
        end % end-if-~isempty(ellipse_struct(shape_id).X0)
      end % end-for-shape_id
      ellipse_struct(fail_fitting) = [];
      candidates{iter} = ellipse_struct;
      % Calculate cost for each ellipse fit
      if ~out_of_bounds 
        local_cost_mat(iter) = eval_cost(ellipse_struct, yx_shape, dims);
      else 
        local_cost_mat(iter) = inf;
      end % end-if-out-of-bounds
    end % end-for-iter
    % Find the min cost local ellipse model 
    local_model_cost = min(local_cost_mat);
    select_id = find(local_cost_mat == local_model_cost);
    models{s_id} = candidates{select_id(1)};
    cost_mat(s_id) = local_model_cost;
  end % end-for-s_id
  % Display the best local ellipse model
  if params.VISUAL
    mask(mask > 0) = 1;
    figure; imagesc(scwhiten(mask));
    hold on;
    for s_id = 1:num_shapes
      model = models{s_id};
      % Draw each sub shape ellipse 
      for ss_id = 1:numel(model)
        try
          X0 = model(ss_id).X0_in;
          Y0 = model(ss_id).Y0_in;
          major_axis = model(ss_id).a;
          minor_axis = model(ss_id).b;
          phi = model(ss_id).phi;
          [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);
          plot(X, Y, 'ko', 'MarkerSize', 5, 'MarkerFaceColor', 'k')
        catch e; keyboard; end;
      end
    end
  end % end-if-params.VISUAL

end % end-function

function cost = eval_cost(ellipse_struct, shape, dims)
  num_ellipses = numel(ellipse_struct);
  ellipse_plot = zeros(dims);
  fit_cost = 0;
  % Plot the areas of the ellipse
  for e_id = 1:num_ellipses
    X0 = ellipse_struct(e_id).X0_in;
    Y0 = ellipse_struct(e_id).Y0_in;
    major_axis = ellipse_struct(e_id).a;
    minor_axis = ellipse_struct(e_id).b;
    phi = ellipse_struct(e_id).phi;
    X = []; Y = [];
    if ~isempty(X0) && ~isempty(Y0)
      [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi, 'fill', dims);
    end
    % Failure to fit ellipse
    if (isempty(X0) || isempty(Y0)) || ...
       (isempty(X) || isempty(Y)) || ...
       (max(Y) > dims(1) || max(X) > dims(2))
      fit_cost = inf;
      fprintf('WARNING: Failed to fit ellipse onto region.\n');
      fprintf('X0=(%i, %i), Y0=(%i, %i), X=(%i, %i), Y=(%i, %i), max(Y)=%d, max(X)=%d\n', ...
              size(X0, 1), size(X0, 2), ...
              size(Y0, 1), size(Y0, 2), ...
              size(X, 1), size(X, 2), ...
              size(Y, 1), size(Y, 2), ...
              max(Y), max(X))
      break;
    end
    ellipse_plot = ellipse_plot+plot_pixels({[Y, X]}, zeros(dims));
    fit_cost = fit_cost+numel(X)^2;
  end % end-for-e_id
  % Plot the area of shape
  ellipse_plot(ellipse_plot ~= 0) = 1;
  shape_plot = plot_pixels({shape}, zeros(dims));
  coverage_plot = shape_plot-ellipse_plot;
  cov_cost = numel(find(coverage_plot ~= 0));
  % c = c_cov+sum(f_i) for i=[1,n] n ellipses 
  cost = cov_cost^2+fit_cost*num_ellipses;
end % end-function

function yx_plot = single_region(yx_plot)
  stats = regionprops(logical(yx_plot), 'Area', 'PixelIdxList');
  % If there are multiple regions (labeling errors)
  if numel(stats) > 1
    areas = cell2mat({stats.Area});
    pixel_list = {stats.PixelIdxList};
    max_region = find(areas == max(areas));
    for r_id = 1:numel(stats)
      if r_id == max_region
        continue;
      end
      yx_plot(pixel_list{r_id}) = 0;
    end
    [y, x] = find(yx_plot == 1);
    yx = [y, x];
    yx_plot = zeros(size(yx_plot));
    yx_plot(yx2index([1, 1,; size(yx_plot)], yx)) = 1;
  end
end % end-function

