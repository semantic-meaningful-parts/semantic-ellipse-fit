function terminal_set = is_terminal(complexities)
  % Find the complexity of each segment
  num_segments = size(complexities, 2);
  terminal_set = false(1, num_segments);
  % Check each segment to see if connectivity contains 0
  for curr_id = 1:num_segments
    curr_cmplx = complexities(:, curr_id);
    curr_endpoint = find(curr_cmplx == 0);
    % If 0 that means one of it's ends is a terminal point
    if ~isempty(curr_endpoint)
      terminal_set(curr_id) = true;
    end % end-if-~isempty(curr_endpoint)
  end % end-for-curr_id
end % end-function

