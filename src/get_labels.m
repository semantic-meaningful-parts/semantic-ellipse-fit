function labels = get_labels(img)
  img(img < 1) = 0;
  label_ids = setdiff(unique(img(:)), 0);
  num_labels = numel(label_ids);
  % Extract the labels
  labels = cell(1, num_labels);
  for i = 1:num_labels
    label_id = label_ids(i);
    ind_label = find(img == label_id);
    yx_label = index2yx([1, 1; size(img)], ind_label);
    labels{label_id} = yx_label;
  end

  labels(cellfun(@isempty, labels)) = [];
end % end-function

