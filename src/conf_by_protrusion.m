function [patch_assign, protru_bases, conf_plot] = conf_by_protrusion(segment_struct, mask)
  MAX_RADIUS = max(size(mask));
  num_segments = numel(segment_struct);
  params = get_params('conf_by_protrusion');
  % Get all the segments
  segment_masters = {segment_struct.master};
  segment_peripherals = {segment_struct.peripherals};
  segment_is_terminal = {segment_struct.is_terminal};
  segment_contours = {segment_struct.contour};
  segment_patches = {segment_struct.patch};
  segment_terminals = find([segment_struct.is_terminal] == true);
  conf_patches = cell(1, num_segments);
  % Find complex junctions by locating those with multiple segments
  segment_sets = cell(1, num_segments);
  % Join the segment masters and peripherals together
  for s_id = 1:num_segments
    segment_sets{s_id} = [segment_masters{s_id}, segment_peripherals{s_id}];
  end
  % Concatenate all segment contours to produce full contour
  full_contour = [];
  for c_id = 1:num_segments
    full_contour = [full_contour; segment_contours{c_id}];
  end
  % Find the set of junctions connecting the segments
  junctions = find_junctions(segment_sets, size(mask), params.NEIGHBOR_RADIUS);
  G = to_adj_graph(segment_masters, params.NEIGHBOR_RADIUS, 'segments');
  [max_flow_node, io_paths, ~] = max_flow_adj(G);
  % Iterate through the complex junctions to find the segments it connects
  keys = junctions.keys;
  num_keys = numel(keys);
  % Initialize output
  protru_bases = cell(1, num_segments); 
  patch_assign = cell(1, num_segments);
  for patch_id = 1:num_segments
    patch = segment_patches{patch_id};
    patch_assign{patch_id} = ones(1, size(patch, 1));
  end
  % Iterate through each key (junction)
  for key_id = 1:num_keys
    key = keys{key_id};
    yx_junction = index2yx([1, 1; size(mask)], key);
    % Get the segments connected to this junction
    values = junctions(key);
    num_values = numel(values);
    % Find the number of segments from each is connected to the junction
    num_connected = zeros(1, num_values);
    for value_id = 1:num_values
      value = values(value_id);
      segment_set = segment_sets{value};
      num_connected(value_id) = num_at_junction(segment_set, yx_junction);
    end
    % Get the one that has the most segments joined at the junction
    skip = find(num_connected == max(num_connected));
    if numel(skip) > 1
      % If they are equally well connected, then both needs to be considered
      skip = -1;
    end
    % Get the torso as well
    torso_id = max_flow_node;
    new_node = -1;
    % Iterate through the segments connected at the junction
    for value_id = 1:num_values
      value = values(value_id); % Protruder
      % Skip if it is the not the one protruding or if it is the torso
      if isequal(value_id, skip) || isequal(value, torso_id)
        continue; % These are protrudees
      end
      % Get vector AB by finding the segment in the master branch joined to yx_junction
      master_segments = segment_masters{value};
      [vector_AB, segment_AB]= segment_vector(yx_junction, master_segments, params.SAMPLE_SIZE);
      A = vector_AB(params.first, :);
      % Find the closest point on the contour to get the radius
      yx_contour = segment_contours{value};
      yx_patch = segment_patches{value};
      % Build a kd-tree containing all the points in the mask contour
      kd_tree = vl_kdtreebuild(yx_contour');
      [~, dist] = vl_kdtreequery(kd_tree, yx_contour', yx_junction');
      l_points = []; r_points = [];
      epsilon = 0; full_search = false;
      % Search until we have found a pair of points on both the left and right
      while isempty(l_points) || isempty(r_points)
        radius = sqrt(dist)+epsilon;
        [index, ~] = rangesearch(yx_contour, yx_junction, radius);
        edge_points = yx_contour(cell2mat(index), :);
        % Iterate through the edge points
        num_edge_points = size(edge_points, 1);
        signs_mat = zeros(1, num_edge_points);
        theta_mat = zeros(1, num_edge_points);
        for point_id = 1:num_edge_points
          P = edge_points(point_id, :);
          % Generate the vector AP
          vector_AP = [A ; P];
          % Compute the sign of the determinant of vectors
          signs_mat(point_id) = sign_det_vectors(vector_AB, vector_AP);
          % Generate the vector PB, from the junction to a given point P
          vector_PB = [P; yx_junction];
          % Meausure the inner angle between our segment and the point P
          theta_mat(point_id) = inner_angle(vector_AB, vector_PB);
        end % end-for-point_id
        invalid_points = find(theta_mat > 90);
        signs_mat(invalid_points) = nan;
        % Separate the points to the left or right of the segment
        l_points = find(signs_mat == -1);
        r_points = find(signs_mat == 1);
        epsilon = epsilon+params.STEP_SIZE;
        if epsilon >= MAX_RADIUS
          fprintf('WARNING: Search range exceeds image size using partial contour, but no points found\n');
          fprintf('DEBUG: segment_id=%i\n', values(:));
          if ~full_search
            epsilon = 0; full_search = true;
            yx_contour = full_contour;
            % Rebuild KD-Tree
            kd_tree = vl_kdtreebuild(yx_contour');
            fprintf('Retrying with full contour search...\n');
          else
            fprintf('WARNING: Search range exceeds image size using full contour, but no points found\n');
            break;
          end
        end
      end % end-while-(isempty(l_points) || isempty(r_points))
      num_left = numel(l_points);
      num_right = numel(r_points);
      if num_left == 0 || num_right == 0
        continue;
      end 
      convexity_mat = nan(num_left, num_right);
      % Find the convexity of the left and right points
      for l_id = 1:num_left
        l_point = edge_points(l_points(l_id), :);
        vector_L = [l_point; yx_junction];
        for r_id = 1:num_right
          r_point = edge_points(r_points(r_id), :);
          vector_R = [r_point; yx_junction];
          % Compute the inner angle as the convexity between two points
          convexity_mat(l_id, r_id) = inner_angle(vector_L, vector_R);
        end % end-for-r_id
      end % end-for-l_id
      % Get the most convex or the minimal inner angle
      [l, r] = find(convexity_mat == max(convexity_mat(:)));
      % Establish the base of the protrusion
      l_base = edge_points(l_points(l(params.first)), :);
      r_base = edge_points(r_points(r(params.first)), :);
      protru_bases{value} = [protru_bases{value}; [l_base; r_base]];
      % Vector starting from midpoint to end of protrusion (opposite of junction)
      vector_LA = [l_base; segment_AB(params.first, :)];
      vector_LR = [l_base; r_base];
      p_sign = sign_det_vectors(vector_LR, vector_LA);
      % Find the sign of the determinant of vector of each mask pixel
      num_pixels = size(yx_patch, 1);
      beta_mat = nan(1, num_pixels);
      for p_id = 1:num_pixels
        P = yx_patch(p_id, :);
        vector_LP = [l_base; P];
        beta_mat(p_id) = sign_det_vectors(vector_LR, vector_LP);
      end
      beta_mat(beta_mat == 0) = p_sign;
      beta_mat(beta_mat ~= p_sign) = 0;
      beta_mat(beta_mat == p_sign) = 1;
      patch_assign{value} = patch_assign{value} & beta_mat;
      % Reassign the pixels
      low_conf = find(beta_mat == 0);
      migrate = yx_patch(low_conf, :);
      if ismember(torso_id, values)
        ee_id  = torso_id(1);
      elseif ~isequal(skip, -1)
        ee_id = values(skip);
      elseif new_node ~= -1
        ee_id = new_node;
      else
        conf_patches{numel(conf_patches)+1} = [];
        new_node = numel(conf_patches);
        ee_id = new_node;
      end
      conf_patches{ee_id} = [conf_patches{ee_id}; migrate];
    end % end-for-value_id
  end % end-for-key_id
  % Plot the points 
  conf_plot = zeros(size(mask));
  for p_id = 1:numel(conf_patches)
    if p_id <= num_segments
      assignment = patch_assign{p_id};
      retained = find(assignment == 1);
      patch = segment_patches{p_id};
      patch = patch(retained, :);
    else
      patch = [];
    end
    conf_patch = conf_patches{p_id}; 
    patch = [patch; conf_patch];
    patch_ind = sub2ind(size(mask), patch(:, 1), patch(:, 2));
    conf_plot(patch_ind) = p_id;
  end
end % end-function

function cnt = cnt_connections(junctions, num_buckets) 
  cnt = zeros(1, num_buckets);
  value_sets = junctions.values;
  num_sets = numel(value_sets);
  for set_id = 1:num_sets
    values = value_sets{set_id};
    num_values = numel(values);
    for value_id = 1:num_values
      value = values(value_id);
      cnt(value) = cnt(value)+1;
    end % end-for-value_id
  end % end-for-set_id
end % end-function

function cnt = num_at_junction(segments, junction)
  num_segments = numel(segments);
  cnt = 0;
  for segment_id = 1:num_segments
    segment = segments{segment_id};
    if pdist([junction; segment(end, :)], 'euclidean') < 3 || ...
       pdist([junction; segment(1, :)], 'euclidean') < 3 
      cnt = cnt+1;
    end
  end
end % end-function



