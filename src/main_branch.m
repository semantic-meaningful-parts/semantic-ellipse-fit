
% Helper function
% Finds the main branch in a list of segments
function master = main_branch(segments)
  num_segments = numel(segments);
  candidates = {segments{1}};
  for segment_id = 2:num_segments
    segment = segments{segment_id};
    seg_front = segment(1, :);
    seg_end = segment(end, :);
    num_candidates = numel(candidates);
    connected = false;
    for candidate_id = 1:num_candidates
      curr_candidate = candidates{candidate_id};
      curr_front = curr_candidate(1, :);
      curr_end = curr_candidate(end, :);
      if isequal(curr_front, seg_front)
        candidates{candidate_id} = [flipud(segment(2:end, :)); curr_candidate];
        connected = true; break;
      elseif isequal(curr_front, seg_end)
        candidates{candidate_id} = [segment(2:end, :); curr_candidate];
        connected = true; break;
      elseif isequal(curr_end, seg_front)
        candidates{candidate_id} = [curr_candidate; segment(2:end, :)];
        connected = true; break;
      elseif isequal(curr_end, seg_end)
        candidates{candidate_id} = [curr_candidate; flipud(segment(2:end, :))];
        connected = true; break;
      end
    end
    if ~connected
      candidates{numel(candidates)+1} = segment;
    end
  end % end-for-segment_id
  % Find the largest branch
  num_candidates = numel(candidates);
  size_mat = zeros(1, num_candidates);
  for candidate_id = 1:num_candidates
    size_mat(candidate_id) = size(candidates{candidate_id}, 1);
  end
  master_id = find(size_mat == max(size_mat));
  master = candidates{master_id};
end % end-function
