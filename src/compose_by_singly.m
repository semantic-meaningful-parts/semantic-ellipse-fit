% compose_by_no_singly
%
% @param segment_struct struct containing segments and their associated data
% @return segment_struct struct r


function segment_struct = compose_by_singly(segment_struct, mask)
  params = get_params('compose_by_singly');

  segment_masters = {segment_struct.master};
  segment_m_endpoints = {segment_struct.m_endpoints};
  segment_peripherals = {segment_struct.peripherals};
  segment_p_endpoints = {segment_struct.p_endpoints};
  segment_is_terminal = {segment_struct.is_terminal};
  segment_patches = {segment_struct.patch};
  num_segments = numel(segment_masters);

  if false %params.DEBUG
    disp_segments = cell(1, numel(segment_masters));
    for disp_seg = 1:numel(segment_masters)
      disp_segment = segment_masters{disp_seg};
      disp_segment = disp_segment{1};
      disp_segments{disp_seg} = disp_segment;
    end
    figure; imagesc(plot_pixels(disp_segments, mask, 'overlay'))
    hold on;
  end

  is_merged = false(1, num_segments);
  G = to_adj_graph(segment_masters, 3, 'segments');

  for curr_id = 1:num_segments
    if is_merged(curr_id)
      continue;
    end
    adj_mat = G(curr_id, :);
    if (sum(adj_mat) == 1)
      select_id = find(adj_mat == 1);
      G(curr_id, select_id) = 0;
      segment_m_endpoints{select_id} = [segment_m_endpoints{select_id}, segment_m_endpoints{curr_id}];
      segment_peripherals{select_id} = [segment_peripherals{select_id}, segment_masters{curr_id}];
      segment_p_endpoints{select_id} = [segment_p_endpoints{select_id}, segment_p_endpoints{curr_id}];
      segment_is_terminal{select_id} = or(segment_is_terminal{select_id}, segment_is_terminal{curr_id});
      segment_patches{select_id} = [segment_patches{select_id}; segment_patches{curr_id}];
      is_merged(curr_id) = true;
    end
  end

    % Update the segment_struct
  for segment_id = 1:num_segments 
    segment_struct(segment_id).master = segment_masters{segment_id};
    segment_struct(segment_id).m_endpoints = segment_m_endpoints{segment_id};
    segment_struct(segment_id).peripherals = segment_peripherals{segment_id};
    segment_struct(segment_id).p_endpoints = segment_p_endpoints{segment_id};
    segment_struct(segment_id).is_terminal = segment_is_terminal{segment_id};
    segment_struct(segment_id).patch = segment_patches{segment_id};
  end % end-for-segment_id
  merged_ids = find(is_merged == true);
  retained = setdiff(1:num_segments, merged_ids);
  segment_struct = segment_struct(retained);
end