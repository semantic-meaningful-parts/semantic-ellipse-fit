% get_relative_junctions
%
% @param part pixel coordinates
% @param junctions junctions of the part
% @return relative_junctions junctions shifted by offset 
%
% Takes a set of pixel coordinates and their junctions and centers
% the junctions based on the offset computed from the pixel coordinates

function index = yx2index(coordinate_plane, coordinates)
  params = get_params('utility');
  % Coordinate plane: [y_min, x_min; y_max, x_max] 
  yx_min = coordinate_plane(1, :);
  yx_max = coordinate_plane(2, :);
  % Number of rows and columns
  y_span = yx_max(params.y)-yx_min(params.y)+1;
  x_span = yx_max(params.x)-yx_min(params.x)+1;
  % Suppose we are on a [1, 1; n, m] plane
  y_offset = 1-yx_min(params.y);
  x_offset = 1-yx_min(params.x);
  yx_offset = repmat([y_offset, x_offset], size(coordinates, 1), 1);
  new_coordinates = coordinates+yx_offset;
  % Compute the index based on the new coordinates
  index = sub2ind([y_span, x_span], new_coordinates(:, params.y), new_coordinates(:, params.x));
end