function segments = merge_simple(segments, RADIUS)
  num_segments = numel(segments);
  % Get the sizes of all segments
  sizes = zeros(1, num_segments);
  is_merged = false(1, num_segments);
  for s_id = 1:num_segments
    sizes(s_id) = size(segments{s_id}, 1);
  end
  % Sort the segments according to sizes
  [~, order] = sort(sizes, 'descend');
  segments = segments(order);
  % Merge a given segment with smoothest 
  p_id = 1;
  while p_id <= num_segments
    % Skip is already merged
    if is_merged(p_id)
      p_id = p_id+1;
      continue;
    end
    theta_mat = zeros(1, num_segments);
    for q_id = p_id:num_segments
      if is_merged(q_id) || p_id == q_id
        continue;
      end
      % Get the next segment we will look at 
      p_segment = segments{p_id};
      q_segment = segments{q_id};
      [p_segment, q_segment] = get_connected({p_segment}, ...
                                             {q_segment}, ...
                                             RADIUS);
      if ~isempty(p_segment) && ~isempty(q_segment)
        theta = inner_angle(p_segment, q_segment);
        theta_mat(q_id) = theta;   
      end
    end % end-for-q_id
    % Do not consider the segments already merged
    merged_id = find(is_merged == true);
    theta_mat(merged_id) = 0;
    % If there isn't a segment to merge
    if max(theta_mat) == 0
      p_id = p_id+1;
      continue;
    end
    % Get the best segment to merge
    select_id = find(theta_mat == max(theta_mat));
    [p_segment, s_segment] = get_connected({segments{p_id}}, ...
                                           {segments{select_id}}, ...
                                           RADIUS);
    segments{p_id} = [p_segment; flipud(s_segment)];
    is_merged(select_id) = true;
  end
  retained = find(is_merged == false);
  segments = segments(retained);
end