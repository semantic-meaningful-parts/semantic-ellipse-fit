% Helper function
% Computes the inner angle between the sets of points A and B
% vector V = (v1, v2), v2 is where the two vectors meet
function theta = inner_angle(A, B, opt)
  if ~exist('opt', 'var') || isempty(opt)
    opt = 'atan2';
  end
  vector_A = [A(end, 1)-A(1, 1), A(end, 2)-A(1, 2)];
  vector_B = [B(end, 1)-B(1, 1), B(end, 2)-B(1, 2)];
  %numer = abs((dot(vector_A, vector_B)));
  %denom = sqrt((vector_A(1)^2)+(vector_A(2)^2))*sqrt((vector_B(1)^2)+(vector_B(2)^2));
  if strcmp(opt, 'cos')
    numer = dot(vector_A, vector_B);
    denom = norm(vector_A)*norm(vector_B);
    quot = numer/denom;
    theta = acosd(quot);
  elseif strcmp(opt, 'atan2')
    vector_A = [vector_A, 0];
    vector_B = [vector_B, 0];
    theta = atan2d(norm(cross(vector_A, vector_B)),dot(vector_A, vector_B));
  end
end