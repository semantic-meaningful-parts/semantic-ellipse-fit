function segment_struct = compose_by_gradient(segment_struct, mask)
  params = get_params('compose_by_gradient');
  MIN_AFFINITY = params.CONTOUR_AFFINITY_THRESH+params.SYMM_AXIS_AFFINITY_THRESH;
  num_segments = numel(segment_struct);
  is_merged = false(1, num_segments);
  % Need the segments and their junctions to measure protrusion
  segment_masters = {segment_struct.master};
  segment_m_endpoints = {segment_struct.m_endpoints};
  segment_peripherals = {segment_struct.peripherals};
  segment_p_endpoints = {segment_struct.p_endpoints};
  segment_is_terminal = {segment_struct.is_terminal};
  segment_contours = {segment_struct.contour};
  segment_contour_plots = {segment_struct.contour_plot};
  segment_patches = {segment_struct.patch};
  max_size = 0;
  for s_id = 1:num_segments
    max_size = max_size+size(segment_patches{s_id}, 1);
  end
  % Iterate through general segments and merge when given large inner angle
  for curr_id = 1:num_segments
    % Skip is already merged
    if is_merged(curr_id)
      continue;
    end
    % Get the current segment
    curr_masters = segment_masters{curr_id};
    curr_master = main_branch(curr_masters);
    % Store the angle/affinity
    theta_mat = zeros(1, num_segments);
    rho_mat = zeros(1, num_segments);
    % Record the current size of patch
    size_mat = nan(1, num_segments);
    % Iterate through all of the segments
    for next_id = curr_id+1:num_segments
      if is_merged(next_id)
        continue;
      end
      % Get the next segment we will look at 
      next_masters = segment_masters{next_id};
      size_mat(next_id) = size(segment_patches{next_id}, 1)+size(segment_patches{curr_id}, 1);;
      next_master = main_branch(next_masters);
      % Get the samples from the two segments that connect
      [curr_sample, next_sample] = sample_segments({curr_master}, ...
                                                   {next_master}, ...
                                                   params.NEIGHBOR_RADIUS, ...
                                                   params.STEP_SIZE, ...
                                                   params.THETA_THRESHOLD, ...
                                                   params.WINDOW_SPAN);
      % If they are connected, see if they should be merged
      if ~isempty(curr_sample) && ~isempty(next_sample)
        % Measure the inner angle of the samples
        theta = inner_angle(curr_sample, next_sample);
        theta_mat(next_id) = theta;
        % Add another measurement based on edge contour
        contour_A = segment_contours{curr_id};
        contour_B = segment_contours{next_id};
        [rho, lr_A, lr_B] = contour_affinity(curr_sample, ...
                                             contour_A, ...
                                             next_sample, ...
                                             contour_B, ...
                                             params.NEIGHBOR_RADIUS);
        if params.VISUAL
          figure; imagesc(mask); hold on; 
          l_A = lr_A{1}; r_A = lr_A{2};
          plot(l_A(:, 2), l_A(:, 1), 'yo', 'MarkerSize', 2);
          plot(r_A(:, 2), r_A(:, 1), 'go', 'MarkerSize', 2);
          l_B = lr_B{1}; r_B = lr_B{2};
          plot(l_B(:, 2), l_B(:, 1), 'wo', 'MarkerSize', 2);
          plot(r_B(:, 2), r_B(:, 1), 'ro', 'MarkerSize', 2);
        end
        rho_mat(next_id) = rho;
      end % end-if-connected
    end % end-for-next_id
    % Get the scores of the affinities based on symmetry axis and contour
    affinity_mat = theta_mat+rho_mat;
    theta_indicators = (theta_mat > params.SYMM_AXIS_AFFINITY_THRESH);
    rho_indicators = (rho_mat > params.CONTOUR_AFFINITY_THRESH);
    indicator_mat = theta_indicators&rho_indicators;
    %affinity_mat = symm_axis_affinity_mat;
    %MIN_AFFINITY = params.SYMM_AXIS_AFFINITY_THRESH;
    % Do not consider the ones that have been merged already
    merged_id = find(is_merged == true);
    affinity_mat(merged_id) = 0;
    indicator_mat(merged_id) = 0;
    % Get the element with the highest affinity
    indicators = find(indicator_mat == true);
    affinity_mat(setdiff(1:num_segments, indicators)) = 0;
    prob_size_mat = exp(-(size_mat)/(max_size*params.SIZE_PERCENT));
    prob_affinity_mat = prob_size_mat.*indicator_mat;
    max_affinity_prob = max(prob_affinity_mat);
    if isnan(max_affinity_prob) | max_affinity_prob < params.MIN_PROBABILITY
      continue;
    end
    select_id = find(prob_affinity_mat == max_affinity_prob);
    % Merge the current segment and its contours with that with max_id
    segment_masters{select_id} = [segment_masters{select_id}, segment_masters{curr_id}];
    segment_m_endpoints{select_id} = [segment_m_endpoints{select_id}, segment_m_endpoints{curr_id}];
    segment_peripherals{select_id} = [segment_peripherals{select_id}, segment_peripherals{curr_id}];
    segment_p_endpoints{select_id} = [segment_p_endpoints{select_id}, segment_p_endpoints{curr_id}];
    segment_is_terminal{select_id} = or(segment_is_terminal{select_id}, segment_is_terminal{curr_id});
    segment_contours{select_id} = [segment_contours{select_id}; segment_contours{curr_id}];
    segment_contour_plots{select_id} = segment_contour_plots{select_id}+segment_contour_plots{curr_id};
    segment_patches{select_id} = [segment_patches{select_id}; segment_patches{curr_id}];
    is_merged(curr_id) = true;
  end % end-for-curr_id
  % Update the segment_struct
  for segment_id = 1:num_segments 
    segment_struct(segment_id).master = segment_masters{segment_id};
    segment_struct(segment_id).peripherals = segment_peripherals{segment_id};
    segment_struct(segment_id).p_endpoints = segment_p_endpoints{segment_id};
    segment_struct(segment_id).is_terminal = segment_is_terminal{segment_id};
    segment_struct(segment_id).contour = segment_contours{segment_id};
    segment_struct(segment_id).contour_plot = segment_contour_plots{segment_id};
    segment_struct(segment_id).patch = segment_patches{segment_id};
  end % end-for-segment_id
  % Get rid of the ones we have merged
  merged_ids = find(is_merged == true);
  retained = setdiff(1:num_segments, merged_ids);
  segment_struct = segment_struct(retained);
end % end-function

% Helper function
% Computes the inner angle between the contours of A and B
function [rho, lr_A, lr_B] = contour_affinity(A, contour_A, B, contour_B, NEIGHBOR_RADIUS)
  % Get the respective vectors of the symmetry axes of A and B
  symm_joint_AB = A(end, :);
  vector_AJ = [A(1, :); symm_joint_AB];
  vector_BJ = [B(1, :); symm_joint_AB];
  % Divide the points of contour A to right and left
  num_contour_A = size(contour_A, 1);
  signs_mat_A = zeros(1, num_contour_A);
  for a_id = 1:num_contour_A
    P = contour_A(a_id, :);
    vector_AP = [A(1, :); P];
    % Compute the sign of the determinant of vectors
    signs_mat_A(a_id) = sign_det_vectors(vector_AJ, vector_AP);
  end
  % Separate the points to the left or right of the segment
  left_A_ids = find(signs_mat_A == -1);
  right_A_ids = find(signs_mat_A == 1);
  lr_A = cell(1, 2); % LEFT=1 RIGHT=2
  lr_A{1} = contour_A(left_A_ids, :);
  lr_A{2} = contour_A(right_A_ids, :);
  % Repeat for B
  num_contour_B = size(contour_B, 1);
  signs_mat_B = zeros(1, num_contour_B);
  for b_id = 1:num_contour_B
    P = contour_B(b_id, :);
    vector_BP = [B(1, :); P];
    % Compute the sign of the determinant of vectors
    signs_mat_B(b_id) = sign_det_vectors(vector_BJ, vector_BP);
  end
  % Separate the points to the left or right of the segment
  left_B_ids = find(signs_mat_B == -1);
  right_B_ids = find(signs_mat_B == 1);
  lr_B = cell(1, 2); % LEFT=1 RIGHT=2
  lr_B{1} = contour_B(left_B_ids, :);
  lr_B{2} = contour_B(right_B_ids, :);
  % Find the joint of the contour
  dist_mat_B = nan(1, num_contour_B);
  ref_mat_A = nan(1, num_contour_A);
  kd_tree = vl_kdtreebuild(contour_A');
  % Compute shortest distance
  for b_id = 1:num_contour_B
    point_B = contour_B(b_id, :);
    [index, dist] = vl_kdtreequery(kd_tree, contour_A', point_B');
    dist_mat_B(b_id) = dist;
    ref_mat_A(b_id) = index;
  end
  % Find the points that joins the contours
  candidates_B = find(dist_mat_B <= NEIGHBOR_RADIUS);
  candidates_A = ref_mat_A(candidates_B);
  num_candidates = numel(candidates_A);
  sign_candidates_A = zeros(1, num_candidates);
  sign_candidates_B = zeros(1, num_candidates);  
  if isempty(sign_candidates_A) || isempty(sign_candidates_B)
    rho = 0;
    return;
  end
  for c_id = 1:num_candidates
    % Identify the side that these points are on 
    if ismember(candidates_A(c_id), left_A_ids)
      sign_candidates_A(c_id) = 1;
    else
      sign_candidates_A(c_id) = 2;
    end
    % Do the same for B
    if ismember(candidates_B(c_id), left_B_ids)
      sign_candidates_B(c_id) = 1;
    else
      sign_candidates_B(c_id) = 2;
    end
  end
  % Compute the inner angles of the connect components of A and B
  rho_mat = nan(1, num_candidates);
  for c_id = 1:num_candidates
    joint_AB = contour_A(candidates_A(c_id), :);
    % Find the other end of A on the given side
    members_A = lr_A{sign_candidates_A(c_id)};
    max_members_A = size(members_A, 1);
    kd_tree_A = vl_kdtreebuild(members_A');
    [index_A, ~] = vl_kdtreequery(kd_tree_A, members_A', joint_AB', ...
                                  'NumNeighbors', max_members_A);
    vector_AJ = [members_A(index_A(max_members_A), :); joint_AB];
    % Do the same for B
    members_B = lr_B{sign_candidates_B(c_id)};
    max_members_B = size(members_B, 1);
    kd_tree_B = vl_kdtreebuild(members_B');
    [index_B, ~] = vl_kdtreequery(kd_tree_B, members_B', joint_AB', ...
                                  'NumNeighbors', max_members_B);
    vector_BJ = [members_B(index_B(max_members_B), :); joint_AB];
    rho_mat(c_id) = inner_angle(vector_AJ, vector_BJ);
  end
  rho = min(rho_mat);
end % end-function
