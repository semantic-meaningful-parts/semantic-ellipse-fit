function [peak, dist] = find_peak(point_A, points, contour, SAMPLE_SIZE)
  MAX_DIST = 50; 
  MAX_Y = max(contour(:, 1)); MIN_Y = min(contour(:, 1));
  MAX_X = max(contour(:, 2)); MIN_X = min(contour(:, 2));
  % Select the other point
  point_B = []; vector_BM = [];
  SAMPLE_SIZE = min(size(points, 1)-1, SAMPLE_SIZE);
  if pdist([point_A; points(end, :)], 'euclidean') < 3
    point_B = points(1, :);
    vector_BM = [point_B; points(SAMPLE_SIZE, :)];
  elseif pdist([point_A; points(1, :)], 'euclidean') < 3
    point_B = points(end, :);
    vector_BM = [point_B; points(end-SAMPLE_SIZE, :)];
  end % end-if-isequal
  % Get the direction of the vector
  if isequal(size(vector_BM), [0,0])
    peak = [];
    dist = -1;
    return;
  end
  y_comp = vector_BM(1, 1)-vector_BM(2, 1);
  x_comp = vector_BM(1, 2)-vector_BM(2, 2);
  theta = atan2d(y_comp, x_comp);
  kd_tree = vl_kdtreebuild(contour');
  d = 1; peak = [];
  while(true)
    y = d*sind(theta);
    x = d*cosd(theta);
    % Find the next point
    point_P = [ceil(point_B(1)+y), ceil(point_B(2)+x)];
    % Look up the closest point to P
    [index, sqr_dist] = vl_kdtreequery(kd_tree, contour', point_P');
    % If it is a neighbor
    if sqrt(sqr_dist) < 3
      % Then we select this point as our peak
      peak = contour(index, :);
      break;
    end % end-if-sqrt(dist)
    % If distance traverse becomes too far
    if d > MAX_DIST || ...
      (point_P(1) > MAX_Y || point_P(1) < MIN_Y) || ...
      (point_P(2) > MAX_X || point_P(2) < MIN_X)
      break;
    end
    % Increment d
    d = d+1;
  end % end-while
  % Check if we have found the peak
  if isempty(peak)
    peak = point_B;
  end
  % Compute the distance from point_A to the peak
  dist_AB = size(points, 1);
  dist_BP = pdist([peak; point_B], 'euclidean');
  dist = dist_AB+dist_BP;
end