function [max_flow_node, io_paths, verbose_paths] = max_flow_adj(G)
  num_vertices = size(G, 1);
  io_paths = zeros(1, num_vertices);
  verbose_paths = cell(num_vertices, num_vertices);
  for v = 1:num_vertices
    for u = 1:num_vertices
      if v == u
        continue;
      end
      % Initialize for DFS
      v_node = v;
      v_used = v_node;
      v_stack = v_node;
      % We continue until there is nothing in our stack
      while ~isempty(v_stack) % If stack is empty then u doesn't exist
        % Get the list of adjacent nodes,
        v_adj = G(v_node, :);
        v_nodes = find(v_adj == 1);
        % If we found the node, then add it to the stack and end
        if ismember(u, v_nodes)
          v_stack = [u, v_stack];
          break; 
        end
        % Get rid of all the used nodes
        v_nodes = setdiff(v_nodes, v_used);
        % If all the nodes have been used, then pop from stack
        if isempty(v_nodes)
          v_stack(1) = [];
          if isempty(v_stack)
            break; % Break out of loop if v_stack is empty
          end
          v_node = v_stack(1);
        else % If we still have nodes to explore
          % Add to our stack
          v_node = v_nodes(1);
          v_used = [v_node, v_used];
          v_stack = [v_node, v_stack];
        end
      end % end-while-~isempty(v_stack)
      verbose_paths{v, u} = v_stack;
      %io_paths(v_stack) = io_paths(v_stack)+1;
    end % end-for-u
  end % end-for-v
  % Re-iterate to find shorest path
  for p = 1:num_vertices
    for q = 1:num_vertices
      pq_verbose = verbose_paths{p, q};
      qp_verbose = verbose_paths{q, p};
      if numel(pq_verbose) < numel(qp_verbose)
        v_stack_upper = pq_verbose;
        verbose_paths{q, p} = pq_verbose;
      else
        v_stack_upper = qp_verbose;
        verbose_paths{p, q} = qp_verbose;
      end % end-if-numel(pq_verbose) < numel(qp_verbose)
      io_paths(v_stack_upper) = io_paths(v_stack_upper)+1;
    end % end-for-q
  end % end-for-p
  max_flow_node = find(io_paths == max(io_paths(:)));
end % end-function

% Remove used nodes
function list_A = list_diff(list_A, list_B)
  num_B = numel(list_B);
  for B_id = 1:num_B
    rm_ids = [];
    num_A = numel(list_A);
    for A_id = 1:num_A
      if isequal(list_A(A_id), list_B(B_id))
        rm_ids = [rm_ids, A_id];
      end
    end
    list_A(rm_ids) = [];
  end
end