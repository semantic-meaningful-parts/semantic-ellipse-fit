function G = to_adj_graph(representation, DIST, opt)
  if ~exist('opt', 'var') || isempty(opt)
    if iscell(representation)
      opt == 'segments';
    elseif isnumeric(representation)
      opt == 'masks';
    end
  end
  G = [];
  if strcmp(opt, 'segments')
    G = to_graph_segments(representation, DIST);
  elseif strcmp(opt, 'masks')
    G = to_graph_masks(representation, DIST);
  end
end

function G = to_graph_segments(segment_sets, NEIGHBOR_RADIUS)
  num_sets = numel(segment_sets);
  G = zeros(num_sets, num_sets);
  % Iterate through each set of segment arrays containing segments
  endpoints_unique = [];
  endpoints_set = cell(1, num_sets);
  try
  full_segments = cell(1, num_sets);
  catch e; keyboard; end
  for set_id = 1:num_sets
    segments = segment_sets{set_id};
    try
    full_segments{set_id} = cell2mat(segments(:));
    catch e 
      keyboard
    end
    num_segments = numel(segments);
    endpoints = [];
    % Look at each individual segment
    %for segment_id = 1:num_segments
      %segment = segments{segment_id};
      % Get the endpoints to all the segments
      %endpoints = [endpoints; segment(1, :); segment(end, :)];
    %end % end-for-segment_id
    %endpoints = unique(endpoints, 'rows');
    %endpoints_set{set_id} = endpoints;
  end % end-for-set_id
  % Populate the shortest distances from set endpoints to global endpoints
  for p_id = 1:num_sets
    %p_endpoints = endpoints_set{p_id};
    p_segment = full_segments{p_id};
    %kd_tree = vl_kdtreebuild(p_endpoints');
    kd_tree = vl_kdtreebuild(p_segment');
    for q_id = 1:num_sets
      if p_id == q_id
        continue;
      end
      %q_endpoints = endpoints_set{q_id};
      q_segment = full_segments{q_id};
      %num_endpoints = size(q_endpoints, 1);
      num_points = size(q_segment, 1);
      %for e_id = 1:num_endpoints
      for e_id = 1:num_points
        %q_endpoint = q_endpoints(e_id, :);
        q_point = q_segment(e_id, :);
        %[~, dist] = vl_kdtreequery(kd_tree, p_endpoints', q_endpoint');
        [~, dist] = vl_kdtreequery(kd_tree, p_segment', q_point');
        % A given endpoint connects a component if dist < NEIGHBOR_RADIUS
        if sqrt(dist) < NEIGHBOR_RADIUS
          G(p_id, q_id) = 1;
          break;
        end
      end % end-for-e_id
    end % end-for-q_id
  end % end-for-p_id
end % end-function

function G = to_graph_masks(label_masks, NEIGHBOR_RADIUS)
  label_ids = setdiff(unique(label_masks(:)), 0);
  num_labels = numel(label_ids);
  G = zeros(num_labels, num_labels);
  labels = cell(1, num_labels);
  % Get all the pixels within a given mask
  for l_id = 1:num_labels
    [y_label, x_label] = find(label_masks == l_id);
    label_plot = plot_pixels({[y_label, x_label]}, label_masks);
    edge_plot = edge(label_plot, 'canny');
    [y_label, x_label] = find(edge_plot > 0);
    labels{l_id} = [y_label, x_label];
  end
  % Generate the adjacency graph for each label
  for p_id = 1:num_labels
    p_label = labels{p_id};
    kd_tree = vl_kdtreebuild(p_label');
    for q_id = 1:num_labels
      % Skip if they are the same label
      if p_id == q_id
        continue; % Will form 0s diagonal
      end
      q_label = labels{q_id};
      num_points = size(q_label, 1);
      for k_id = 1:num_points
        q_point = q_label(k_id, :);
        [~, dist] = vl_kdtreequery(kd_tree, p_label', q_point');
        % If the some points in both labels are next to each other
        if sqrt(dist) <= NEIGHBOR_RADIUS
          G(p_id, q_id) = 1; % They must be adjacent
          break; % We are done with this label
        end % end-if-sqrt(dist)
      end % end-for-k_id
    end % end-for-q_id
  end % end-for_p_id
end % end-function