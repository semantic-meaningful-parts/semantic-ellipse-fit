function fit_ellipse_model(mask, MAX_ITER)
  % Get internal parameters 
  params = get_params('ellipse_model');
  params.VISUAL = true;
  dims = size(mask);
  % Get the indices for the shape image
  yx_mask = get_labels(mask);
  yx_mask = yx_mask{1};
  % Get the major symmetry axis of this shape
  tmp_mask = zeros(dims);
  tmp_mask(yx2index([1, 1; dims], yx_mask)) = 1;
  tmp_mask = single_region(tmp_mask);
  [y_mask, x_mask] = find(tmp_mask == 1);
  yx_mask = [y_mask, x_mask];
  skeleton = symmetry_axis(tmp_mask, 'default');

  % Decompose skeleton into segments
  shape_axes = decompose2segments(skeleton);
  % For each symmetry axis, assign 
  % yx_shapes = assign2parts(shape_axes, mask, 'full');
  yx_shapes= assign2parts_msfm(shape_axes, mask);
  if params.VISUAL
    figure; 
    subplot(1, 2, 1);
    imagesc(plot_pixels(shape_axes, zeros(dims)));
    subplot(1, 2, 2);
    shape_masks = plot_pixels(yx_shapes, zeros(dims));
    imagesc(shape_masks);
  end % end-if-params.VISUAL
  num_shapes = numel(yx_shapes);
  ellipse_models = cell(1, num_shapes);
  % Try to fit ellipses for each of the shapes
  for s_id = 1:num_shapes
    shape_axis = shape_axes{s_id};
    yx_shape = yx_shapes{s_id};
    local_cost_mat = zeros(1, MAX_ITER);
    local_candidates = cell(1, MAX_ITER);
    % Try to find the best local fit of ellipses
    for iter = 1:MAX_ITER
      % Perform k-cuts on the shape
      [sub_shapes, sub_axes, yx] = k_cuts(shape_axis, ...
                                          yx_shape, ...
                                          iter-1, ... % 1st iteration should be 0-cut
                                          params.STEP_SIZE, ...
                                          params.WINDOW_SIZE);

      % We could not find anymore locations to cut
      if isempty(sub_axes) 
        local_cost_mat(iter:MAX_ITER) = inf;
        break;
      end
      % Initialize ellipse struct storage
      num_sub_shapes = numel(sub_shapes);
      ellipse_struct = repmat(params.ELLIPSE_STRUCT, 1, num_sub_shapes);
      % Set up visualizations for ellipse fitting
      if params.VISUAL
        figure; imagesc(shape_masks);
        hold on;
      end % end-if-params.VISUAL
      % Fit ellipse on each of the shapes
      out_of_bounds = false;
      for shape_id = 1:num_sub_shapes
        shape_i = sub_shapes{shape_id};
        % Fit ellipse via least-squares
        ellipse_struct(shape_id) = fit_ellipse(shape_i(:, 2), shape_i(:, 1));
        if ~isempty(ellipse_struct(shape_id).X0)
          X0 = ellipse_struct(shape_id).X0_in;
          Y0 = ellipse_struct(shape_id).Y0_in;
          major_axis = ellipse_struct(shape_id).a;
          minor_axis = ellipse_struct(shape_id).b;
          phi = ellipse_struct(shape_id).phi;
          % Visualization
          if params.VISUAL
            [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);
            plot(X, Y, 'ko', 'MarkerSize', 3)
          end % end-if-params.VISUAL
          if (isempty(X0) || isempty(Y0)) || ...
             (isempty(X) || isempty(Y)) || ...
             (max(Y) > dims(1) || max(X) > dims(2))
            % Set flag that out of bounds
            out_of_bounds = true;
            fprintf('WARNING: Points on ellipse out of bounds.\n');
            fprintf('(X0, Y0)=(%i, %i), X=(%i, %i), Y=(%i, %i), max(Y)=%d, max(X)=%d\n', ...
              X0, Y0, ...
              size(X, 1), size(X, 2), ...
              size(Y, 1), size(Y, 2), ...
              max(Y), max(X));
          end
        end % end-if-~isempty(ellipse_struct(shape_id).X0)
      end % end-for-shape_id
      candidates{iter} = ellipse_struct;
      % Calculate cost for each ellipse fit
      if ~out_of_bounds
        local_cost_mat(iter) = eval_cost(ellipse_struct, yx_shape, dims);
      else 
        local_cost_mat(iter) = inf;
      end % end-if-out-of-bounds
    end % end-for-iter
    % Find the min cost local ellipse model 
    local_cost_mat
    local_model_cost = min(local_cost_mat);
    select_id = find(local_cost_mat == local_model_cost);
    ellipse_models{s_id} = candidates{select_id(1)};
  end % end-for-s_id
  % Display the best local ellipse model
  if params.VISUAL
    figure; imagesc(mask);
    hold on;
    for s_id = 1:num_shapes
      model = ellipse_models{s_id};
      % Draw each sub shape ellipse 
      for ss_id = 1:numel(model)
        X0 = model(ss_id).X0_in;
        Y0 = model(ss_id).Y0_in;
        major_axis = model(ss_id).a;
        minor_axis = model(ss_id).b;
        phi = model(ss_id).phi;
        [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);
        plot(X, Y, 'ko', 'MarkerSize', 3)
      end
    end
  end % end-if-params.VISUAL

end


function cost = eval_cost(ellipse_struct, shape, dims)
  num_ellipses = numel(ellipse_struct);
  ellipse_plot = zeros(dims);
  fit_cost = 0;
  % Plot the areas of the ellipse
  for e_id = 1:num_ellipses
    X0 = ellipse_struct(e_id).X0_in;
    Y0 = ellipse_struct(e_id).Y0_in;
    major_axis = ellipse_struct(e_id).a;
    minor_axis = ellipse_struct(e_id).b;
    phi = ellipse_struct(e_id).phi;
    if ~isempty(X0) && ~isempty(Y0)
      [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi, 'fill', dims);
    end
    % Failure to fit ellipse
    if (isempty(X0) || isempty(Y0)) || ...
       (isempty(X) || isempty(Y)) || ...
       (max(Y) > dims(1) || max(X) > dims(2))
%      fit_cost = inf;
      fprintf('WARNING: Failed to fit ellipse onto region.\n');
      fprintf('X0=(%i, %i), Y0=(%i, %i), X=(%i, %i), Y=(%i, %i), max(Y)=%d, max(X)=%d\n', ...
              size(X0, 1), size(X0, 2), ...
              size(Y0, 1), size(Y0, 2), ...
              size(X, 1), size(X, 2), ...
              size(Y, 1), size(Y, 2), ...
              max(Y), max(X))
%      break;
    end
    ellipse_plot = ellipse_plot+plot_pixels({[Y, X]}, zeros(dims));
    fit_cost = fit_cost+numel(X)^2;
  end % end-for-e_id
  % Plot the area of shape
  ellipse_plot(ellipse_plot ~= 0) = 1;
  shape_plot = plot_pixels({shape}, zeros(dims));
  coverage_plot = shape_plot-ellipse_plot;
  cov_cost = numel(find(coverage_plot ~= 0));
  % c = c_cov+sum(f_i) for i=[1,n] n ellipses 
  cost = cov_cost^2+fit_cost*num_ellipses;
end % end-function

function yx_plot = single_region(yx_plot)
  stats = regionprops(logical(yx_plot), 'Area', 'PixelIdxList');
  % If there are multiple regions (labeling errors)
  if numel(stats) > 1
    areas = cell2mat({stats.Area});
    pixel_list = {stats.PixelIdxList};
    max_region = find(areas == max(areas));
    for r_id = 1:numel(stats)
      if r_id == max_region
        continue;
      end
      yx_plot(pixel_list{r_id}) = 0;
    end
    [y, x] = find(yx_plot == 1);
    yx = [y, x];
    yx_plot = zeros(size(yx_plot));
    yx_plot(yx2index([1, 1,; size(yx_plot)], yx)) = 1;
  end
end % end-function
