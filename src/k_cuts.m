function [shapes, axes, yx] = k_cuts(axis, shape, k, STEP_SIZE, WINDOW)
  % Segments that have been decomposed by the change in gradient
  num_points_axis = size(axis, 1); 
  axes = {}; 
  shapes = {};
  yx = [];
  if k == 0
    axes = {axis};
    shapes = {shape};
  end
  % Make sure we have enough pixels to get samples
  if num_points_axis < (2*WINDOW+1)
    return;
  end
  % Get the locations of the axis where local change in gradient is large
  [a_pos, a_theta] = get_cuts(axis, STEP_SIZE, WINDOW);
  if numel(a_theta) < k
    return;
  end
  yx = axis(a_pos, :);
  [~, order] = sort(a_theta, 'descend');
  % Make sure we actually have k cuts
  k_pos = sort(a_pos(order(1:k)), 'ascend');
  arr_pos = 1;
  curr_pos = 1;
  % Cut the axis
  for pos_id = 1:k
    cut_pos = k_pos(pos_id);
    axes{arr_pos}  = axis(curr_pos:cut_pos, :);
    curr_pos = cut_pos;
    arr_pos = arr_pos+1;
  end
  % Last cut must be the ending portion
  axes{arr_pos} = axis(curr_pos:end, :);
  % Assign each edge pixel to a symmetry axis pixel
  kd_tree = vl_kdtreebuild(axis');
  num_points_shape = size(shape, 1);
  axis_assign = zeros(1, num_points_shape);
  for ps_id = 1:num_points_shape
    shape_point = shape(ps_id, :);
    [index, ~] = vl_kdtreequery(kd_tree, axis', shape_point');
    axis_assign(ps_id) = index;
  end
  % Parition the shape by the axis assignments
  num_axes = numel(axes);
  for a_id = 1:num_axes
    curr_axis = axes{a_id};
    axis_members = ismember(axis, curr_axis, 'rows');
    axis_members = find(axis_members == 1);
    assigned_ids = ismember(axis_assign, axis_members);
    shapes{a_id} = shape(assigned_ids, :);
  end
end % end-function

function [cut_pos, theta] = get_cuts(points, STEP_SIZE, WINDOW)
  num_points = size(points, 1);
  % Inner angles for each set of samples
  theta_mat = nan(1, num_points);
  START_POS = STEP_SIZE;
  END_POS = num_points-STEP_SIZE;
  % Traverse the pixels to get 2 vectors
  for p_id = START_POS:END_POS
    % Since segments are all in the order of traversal
    START_A = p_id-STEP_SIZE+1; END_A = p_id;
    START_B = p_id+STEP_SIZE-1; END_B = p_id;
    % Get a sample of the segment
    vector_A = [points(START_A, :); points(END_A, :)];
    vector_B = [points(START_B, :); points(END_B, :)];
    theta_mat(p_id) = inner_angle(vector_A, vector_B);
  end % end-for-p_id
  % Locate the pixels that we can cut
  cut_candidates = find(~isnan(theta_mat));
  num_candidates = numel(cut_candidates);
  for cut_id = 1:num_candidates
    curr_cut = cut_candidates(cut_id);
    window = theta_mat(max(1, curr_cut-WINDOW):min(num_points, curr_cut+WINDOW));
    % Find the max of this window and do non-maximal suppression
    curr_max = min(window); % Smaller the angle, the sharper the curve
    max_pos = find(window == curr_max);
    window(setdiff(1:numel(window), max_pos)) = nan;
    theta_mat(max(1, curr_cut-WINDOW):min(num_points, curr_cut+WINDOW)) = window;
  end % end-for-cut_id
  cut_finalists = find(~isnan(theta_mat));
  % Last pass through for close neighbors
  num_finalists = numel(cut_finalists);
  for nf_id = 1:num_finalists
    f_pos = cut_finalists(nf_id);
    span = theta_mat(max(1, f_pos-STEP_SIZE):min(num_points, f_pos+STEP_SIZE));
    % Find the max of this window and do non-maximal suppression
    curr_max = min(span); % Smaller the angle, the sharper the curve
    if isnan(curr_max)
      continue;
    end
    max_pos = find(span == curr_max);
    span(setdiff(1:numel(span), max_pos(1))) = nan;
    theta_mat(max(1, f_pos-STEP_SIZE):min(num_points, f_pos+STEP_SIZE)) = span;
  end
  cut_pos = find(~isnan(theta_mat));
  theta = theta_mat(cut_pos);
end % end-function


% % Attempt to find curvature in edge,
% % but edge detection is bad due to bad labeling
% % Generate local vectors and determine sign of edge pixels
% start_pos = 1;
% num_cuts = numel(a_pos)+1;
% l_edges = []; r_edges = [];
% for c_id = 1:num_cuts
%   % The last cut will be from the last index to last axis pixel
%   if c_id == num_cuts
%     end_pos = num_points;
%   else
%     end_pos = a_pos(c_id); 
%   end
%   a_indices = start_pos:end_pos;
%   start_pos = end_pos;
%   % Get the edge points associated with the local axis
%   e_assigned = ismember(axis_assign, a_indices);
%   e_assigned = find(e_assigned == 1);
%   e_points = edges(e_assigned, :);
%   % For each edge point, determine whether it is on the left or right
%   num_edge_points = numel(e_assigned);
%   vector_AB = [axis(start_pos, :); axis(end_pos, :)];
%   signs_mat = zeros(1, num_edge_points);
%   for ep_id = 1:num_edge_points
%     vector_AE = [axis(start_pos, :); e_points(ep_id, :)];
%     % Compute the sign of the determinant of vectors
%     signs_mat(ep_id) = sign_det_vectors(vector_AB, vector_AE);
%   end
%   % Separate the points to the left or right of the segment
%   l_indices = find(signs_mat == -1);
%   l_points = e_points(l_indices, :);
%   l_edges = [l_edges; l_points];
%   r_indices = find(signs_mat == 1);
%   r_points = e_points(r_indices, :);
%   r_edges = [r_edges; r_points];
% end
