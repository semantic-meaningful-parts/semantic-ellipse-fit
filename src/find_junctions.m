function junctions = find_junctions(segment_sets, dims, DIST)
  junctions = containers.Map('KeyType', 'double', 'ValueType', 'any');
  num_sets = numel(segment_sets);
  % Iterate through each set of segment arrays containing segments
  endpoints_unique = [];
  endpoints_set = cell(1, num_sets);
  for set_id = 1:num_sets
    segments = segment_sets{set_id};
%    num_segments = numel(segments);
    try
    full_segments{set_id} = cell2mat(segments(:));
    catch e 
      keyboard
    end
%    endpoints = [];
    % Look at each individual segment
%    for segment_id = 1:num_segments
%      segment = segments{segment_id};
      % Get the endpoints to all the segments
%      endpoints = [endpoints; segment(1, :); segment(end, :)];
%    end % end-for-segment_id
%    endpoints = unique(endpoints, 'rows');
%    endpoints_set{set_id} = endpoints;
%    endpoints_unique = [endpoints_unique; endpoints];
  end % end-for-set_id
%  endpoints_unique = unique(endpoints_unique, 'rows');
%  num_unique = size(endpoints_unique, 1);
%  adjacency_mat = zeros(num_sets, num_unique);
  adjacency_mat = zeros(num_sets, num_sets);
  endpoints_mat = cell(num_sets, num_sets);
  % Populate the shortest distances from set endpoints to global endpoints
  %for set_id = 1:num_sets
  for p_id = 1:num_sets
    %s_endpoints = endpoints_set{set_id};
    p_segment = full_segments{p_id};
    %kd_tree = vl_kdtreebuild(s_endpoints');
    kd_tree = vl_kdtreebuild(p_segment');
    % for uniq_id = 1:num_unique
    %   u_endpoint = endpoints_unique(uniq_id, :);
    %   [~, dist] = vl_kdtreequery(kd_tree, s_endpoints', u_endpoint');
    %   % A given endpoint connects a component if dist < DIST
    %   if sqrt(dist) < DIST
    %     adjacency_mat(set_id, uniq_id) = 1;
    %   end
    % end % end-for-uniq_id

    for q_id = 1:num_sets
      if p_id == q_id
        continue;
      end
      %q_endpoints = endpoints_set{q_id};
      q_segment = full_segments{q_id};
      %num_endpoints = size(q_endpoints, 1);
      num_points = size(q_segment, 1);
      %for e_id = 1:num_endpoints
      for e_id = 1:num_points
        %q_endpoint = q_endpoints(e_id, :);
        q_point = q_segment(e_id, :);
        %[~, dist] = vl_kdtreequery(kd_tree, p_endpoints', q_endpoint');
        [index, dist] = vl_kdtreequery(kd_tree, p_segment', q_point');
        % A given endpoint connects a component if dist < NEIGHBOR_RADIUS
        if sqrt(dist) < 3
          adjacency_mat(p_id, q_id) = 1;
          endpoints_mat{p_id, q_id} = p_segment(index, :);
          break;
        end
      end % end-for-e_id
    end % end-for-q_id
  end % end-for-set_id
  % Sum up all columns in each row
  connectivity_mat = sum(adjacency_mat, 2);
  % Populate the junctions
  for p_id = 1:num_sets
    % We skip if only 1 point connected to the endpoint
    if connectivity_mat(set_id) == 1
      continue; % Occurs at endpoints, and previously merged junction
    end
    adjacency_list = adjacency_mat(p_id, :);
    segment_ids = find(adjacency_list == 1);
    for s_id = 1:numel(segment_ids)
      q_id = segment_ids(s_id);
      yx_junction = endpoints_mat{p_id, q_id};
      key = yx2index([1, 1; dims], yx_junction);
      if junctions.isKey(key)
        value = junctions(key);
        if ~ismember(q_id, value)
          junctions(key) = [value; q_id];
        end
      else
        junctions(key) = q_id;
      end
    end
  end % end-for-uniq_id
end % end-function