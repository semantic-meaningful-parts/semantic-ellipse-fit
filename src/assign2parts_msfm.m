%-----------------------------------------------------------------------------
% assign2parts_msfm
%
% Assigns each pixel of a binary mask to a part
%-----------------------------------------------------------------------------
% function assigned_pixels = assign2parts_msfm(parts, mask, opt)
function [pxls_part, pxls_edge, map, map_edge, empty] = assign2parts_msfm(parts, mask)
  num_parts = numel(parts);
  [rows, cols] = size(mask);
  one = ones(rows, cols);
  empty = [];
  % compute distance maps per part
  dmap = zeros(rows,cols,num_parts);
  for part_id = 1:num_parts;
    if isempty(parts{part_id})
      dmap(:, :, part_id) = inf;
      empty = [empty, part_id];
    else
      dmap(:,:,part_id) = msfm(one, parts{part_id}', true, true);
    end
  end

  % get both edge map and filled pixels
  [~, idx] = min(dmap, [], 3);
  map = idx .* double(mask); % full
  labels = unique(map);
  unassigned = setdiff(0:num_parts, labels);
  empty = [empty, unassigned];
  edge_mask = edge(mask, 'sobel');
  map_edge = idx .* double(edge_mask); % edges

  % change to alex's format
  % [~, ~, dxi, dyi] = make_difference_operator([rows,cols]);
  [pxls_part, pxls_edge] = map2fmt(map, map_edge);
end

%-----------------------------------------------------------------------------
% this changes the map to the alex's format
%-----------------------------------------------------------------------------
function [pxls_part, pxls_edge] = map2fmt(map, map_edge)
[rows,cols] = size(map); imsize = [rows,cols]; npx = rows*cols; pxls = 1:npx;
labels = unique(map);
labels(1) = []; % assume and remove label 0 (background)
num_parts = length(labels);
pxls_parts = {};

for k = 1:num_parts;
  [y,x] = ind2sub(imsize, pxls(map(:) == k));
  pxls_part{k} = [y',x'];
  [y,x] = ind2sub(imsize, pxls(map_edge(:) == k));
  pxls_edge{k} = [y',x'];
end
end

