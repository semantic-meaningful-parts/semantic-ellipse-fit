function [A, B] = get_connected(segments_A, segments_B, DIST)
  % Return values
  A = []; B = [];
  % Get all the endpoints for A
  num_A = numel(segments_A);
  endpoints_A = [];
  for A_id = 1:num_A
    segment_A = segments_A{A_id};
    endpoints_A = [endpoints_A; segment_A(1, :); segment_A(end, :)];
  end % end-for-A_id
  endpoints_A = unique(endpoints_A, 'rows');
  num_points_A = size(endpoints_A, 1);
  % Get all the endpoints for B
  num_B = numel(segments_B);
  endpoints_B = [];
  for B_id = 1:num_B
    segment_B = segments_B{B_id};
    endpoints_B = [endpoints_B; segment_B(1, :); segment_B(end, :)];
  end % end-for-B_id
  endpoints_B = unique(endpoints_B, 'rows');
  num_points_B = size(endpoints_B, 1);
  % Generate a list of their shortest distances
  kd_tree = vl_kdtreebuild(endpoints_B');
  dist_mat = zeros(1, num_points_A);
  index_B_mat = zeros(1, num_points_A);
  for q_id = 1:num_points_A
    endpoint_A = endpoints_A(q_id, :);
    [index, dist] = vl_kdtreequery(kd_tree, endpoints_B', endpoint_A');
    dist_mat(q_id) = sqrt(dist);
    index_B_mat(q_id) = index;
  end % end-for-q_id
  % Find the index of B with the shortest distance
  min_dist = min(dist_mat(:));
  if min_dist > DIST
    return;
  end % end-if-min_dist > DIST
  % Select the joints for A and B by the closest junctions between the two
  min_index = find(dist_mat == min_dist);
  min_index = min_index(1);
  min_id = index_B_mat(min_index);
  joint_B = endpoints_B(min_id, :);
  joint_A = endpoints_A(min_index, :);
  % Find the corresponding segment that contains these joints in A
  segment_A_id = [];
  for A_id = 1:num_A
    segment_A = segments_A{A_id};
    if (segment_A(1, :) == joint_A) | (segment_A(end, :) == joint_A)
      segment_A_id = A_id;
      break;
    end
  end % end-for-A_id
  A = segments_A{segment_A_id};
  % Flip segment_A to make sure joint is at end
  if A(1, :) == joint_A
    A = flipud(A);
  end 
  % Find the corresponding segment that contains these joints in A
  segment_B_id = [];
  for B_id = 1:num_B
    segment_B = segments_B{B_id};
    if ismember(joint_B, segment_B, 'rows')
      segment_B_id = B_id;
      break;
    end
  end % end-for-B_id
  B = segments_B{segment_B_id};
  % Flip segment_A to make sure joint is at end
  if B(1, :) == joint_B
    B = flipud(B);
  end
end % end-function