%-----------------------------------------------------------------------------
% Helper function
% Populates segment structure with data
%-----------------------------------------------------------------------------
function segment_struct = populate_segment_struct(segments, mask)
  params = get_params('meaningful_parts');
  segment_struct = params.SEGMENT_STRUCT;
  num_segments  = numel(segments);

  % Assign some part of the contour of the mask to the segments
  TIME_ASSIGN2PARTS = false;
  if ~TIME_ASSIGN2PARTS;
    [patches, contours, map] = assign2parts_msfm(segments, mask);
  else
    ta = tic();
    [patches, contours, map] = assign2parts_msfm(segments, mask); % 1.7 s
    ta = toc(ta); fprintf('assign2parts msfm: %0.4f s\n', ta);
    ta = tic();
    contours2 = assign2parts(segments, mask, 'edge');
    ta = toc(ta); fprintf('assign2parts edge: %0.4f s\n', ta); % ~4.3 s
    ta = tic();
    patches2 = assign2parts(segments, mask, 'full'); % ~ 104 s
    ta = toc(ta); fprintf('assign2parts full: %0.4f s\n', ta);

    % comparison: msfm vs kdtree approach
    map2 = zeros(size(mask)); map = double(map);
    for p_id = 1:numel(patches);
      patch = patches{p_id};
      patch_ind = sub2ind(size(mask), patch(:, 1), patch(:, 2));
      map(patch_ind) = p_id;
    end
    fig(55); 
    subplot(2,3,1); imagex(map2); title('patch -- original');
    subplot(2,3,2); imagex(map); title('msfm');
    subplot(2,3,3); imagex(map2-map); err = sum(sum(abs(map2-map) >= 1)); 
    title(sprintf('number of different pixels: %d', err));

    cont = zeros(size(mask)); cont2 = zeros(size(mask));
    for p_id = 1:numel(contours);
      contour = contours{p_id};
      if ~isempty(contour);
        contour2 = contours2{p_id};
        contour_ind = sub2ind(size(mask), contour(:, 1), contour(:, 2));
        contour_ind2 = sub2ind(size(mask), contour2(:, 1), contour2(:, 2));
        cont(contour_ind) = p_id; cont2(contour_ind) = p_id;
      end
    end
    subplot(2,3,4); imagex(cont2); title('contour -- original');
    subplot(2,3,5); imagex(cont); title('msfm');
    subplot(2,3,6); imagex(cont2-cont); err = sum(sum(abs(cont2-cont) >= 1)); 
    title(sprintf('number of different pixels: %d', err));
  end

  % Check if each segment contains a terminal point
  complexities = joint_complexity(segments);
  terminals = is_terminal(complexities);
  % Iterate through each segment and populate the segment struct
  for segment_id = 1:num_segments
    segment = segments{segment_id};
    contour = contours{segment_id};
    patch = patches{segment_id};
    segment_struct(segment_id).master = {segment};
    segment_struct(segment_id).m_endpoints = [segment(params.first, :); segment(end, :)];
    segment_struct(segment_id).peripherals = {}; % Initialize with no peripherals
    segment_struct(segment_id).p_endpoints = {}; % Hence no peripheral endpoints
    segment_struct(segment_id).is_terminal = terminals(segment_id);
    segment_struct(segment_id).contour = contour;
    segment_struct(segment_id).contour_plot = plot_pixels({contour}, mask);
    segment_struct(segment_id).patch = patch; 
  end % end-for-segment_id
end % end-function
