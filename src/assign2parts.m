% Helper function
% Assigns each pixel of a binary mask to a part
function assigned_pixels = assign2parts(parts, mask, opt)
  num_parts = numel(parts);
  % Get the list of symmetry axis pixels
  symm_pixels = [];
  for part_id = 1:num_parts
    symm_pixels = [symm_pixels; parts{part_id}];
  end % end-for-part_id
  symm_pixels = unique(symm_pixels, 'rows');
  num_symm_pixels = size(symm_pixels, 1);
  y_mask_pixels = []; x_mask_pixels = [];
  % Train a KD-tree here with symm_pixels
  symm_tree = vl_kdtreebuild(symm_pixels');
  % Get the list of pixels from mask
  if strcmp(opt, 'full')
    [y_mask_pixels, x_mask_pixels] = find(mask > 0);
  elseif strcmp(opt, 'edge')
    edge_mask = edge(mask, 'sobel');
    [y_mask_pixels, x_mask_pixels] = find(edge_mask > 0);
  end
  mask_pixels = [y_mask_pixels, x_mask_pixels];
  num_mask_pixels = size(mask_pixels, 1);
  % Compute distance of each mask_pixels to symm_pixels
  assigned_pixels = cell(1, num_parts);
  for mask_pixel_id = 1:num_mask_pixels
    mask_pixel = mask_pixels(mask_pixel_id, :);
    dists = zeros(num_symm_pixels, 1);
    % Adding a KD tree query here
    [index, dists] = vl_kdtreequery(symm_tree, ...
                                    symm_pixels', mask_pixel', ...
                                    'NumNeighbors', num_parts);
    % Find the minimum distance and corresponding pixels
    min_dist = min(dists(:));
    min_dist_symm = find(dists == min_dist);
    num_mins = size(min_dist_symm, 1);
    % Locate the part(s) these pixels belong to
    for min_id = 1:num_mins
      min_position = index(min_dist_symm(min_id));
      min_pixel = symm_pixels(min_position, :);
      for part_id = 1:num_parts
        part = parts{part_id};
        % If the given symm_pixel belongs to a part
        if ismember(min_pixel, part, 'rows')
          % Add the mask_pixel to the assigned_pixels of part
          part_pixels = assigned_pixels{part_id};
          assigned_pixels{part_id} = [part_pixels; mask_pixel];
        end % end-if-ismember
      end % end-for-part_id
    end % end-for-min_id
  end % end-for-mask_pixel_id
end % end-function
