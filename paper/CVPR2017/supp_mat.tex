\documentclass[10pt,onecolumn,letterpaper]{article}

\usepackage{cvpr}
\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}

\newcommand{\figref}[1]{Fig. \ref{#1}}
\newcommand{\tabref}[1]{Table \ref{#1}}
\newcommand{\secref}[1]{Sec. \ref{#1}}
\newcommand{\eqnref}[1]{Eqn. \ref{#1}} 
\newcommand{\algref}[1]{Alg. \ref{#1}}

% Include other packages here, before hyperref.

% If you comment hyperref and then uncomment it, you should delete
% egpaper.aux before re-running latex.  (Or just hit 'q' on the first latex
% run, let it finish, and you should be clear).
\usepackage[breaklinks=true,bookmarks=false]{hyperref}

%\cvprfinalcopy % *** Uncomment this line for the final submission

\def\cvprPaperID{****} % *** Enter the CVPR Paper ID here
\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}

% Pages are numbered in submission mode, and unnumbered in camera-ready
\ifcvprfinal\pagestyle{empty}\fi
\begin{document}

%%%%%%%%% TITLE
\title{Fast and Efficient Ellipse Model \\ SUPPLEMENTARY MATERIALS}

\author{
Alex Wong \\
Department of Computer Science\\
University of California, Los Angeles\\
\texttt{alexw@cs.ucla.edu} \\
%% examples of more authors
\and
Brian Taylor \\
Department of Computer Science \\
University of California, Los Angeles \\
\texttt{btay@cs.ucla.edu} \\
\and
Alan Yuille \\
Department of Computer Science \\
Johns Hopkins University \\
\texttt{alan.yuille@jhu.edu } \\
 }


\maketitle
%\thispagestyle{empty}

\begin{figure} [h!]
\centering
\includegraphics[width=1\textwidth]{figures/k_cuts_pipeline_square}
\caption{Examples of the local ellipse fitting procedure using $\kappa-1$ cuts. Given the intermediary results after applying protrusion cues, we first find the symmetric axis $\mathbf{A}_j$. We then perform $\kappa-1$ cuts along $\mathbf{A}_j$ to produce $\kappa$ regions. A candidate ellipse model is proposed for each value of $\kappa$, which typically ranges from 1 to 5. For each region, the local ellipse model that minimizes Eqn. 2 from the main paper is selected as the representation.}
\label{k_cuts_pipeline}
\end{figure}

\section{Local Ellipse Fitting with $\kappa-1$ cuts}
In the main paper, we described our local ellipse fitting method that performs $\kappa-1$ cuts along the symmetric axis of a region to produce $\kappa$ subregions, to each of which we fit an ellipse. We first find the longest continuous segment through the major axis of the region and denote it as the symmetric axis $\mathbf{A}_j$. Next, we initialize $\kappa$ to 1 and increase it by 1 on every iteration to make $\kappa-1$ cuts. We perform local ellipse fitting via a least-square estimator to generate a set of candidate ellipses to represent the region. The procedure terminates when any of the ellipses falls outside of the image domain or when the least-square fitting fails due to matrix inversion. We then compute the ellipse fitting cost (Eqn. 2 from the main paper) for each set of candidate ellipses and select the model that minimizes the cost. This procedure is illustrated in \figref{k_cuts_pipeline}.


\section{Additional results}
In the experiment section, we reported results on the task of ellipse fitting on over 4000 2D shapes of different objects, sizes and orientations from four different datasets: LEMS, MPEG-7, SiSHA, and PASCAL Horses. Our shape coverage as measured by Intersection-Over-Union (IOU) is shown to be comparable to that of the Augmentative Ellipse Fitting Algorithm (AEFA) and the Decremental Ellipse Fitting Algorithm (DEFA) from \cite{panagiotakis2015parameter} and consistently outperforms the EM algorithm proposed by \cite{xu2010fitting}. Sample output from all of the methods evaluated in this paper is shown in \figref{supp_comparisons} for comparison. For the experiments, we allow each method to choose its own optimal number of ellipses (based on its model selection criterion) to fit a given shape. Due to similar initialization steps, our method generates results similar to that of AEFA and DEFA. However, our method is much faster than all of the competing methods. 

\begin{figure} [h!]
\centering
\includegraphics[width=0.75\textwidth]{figures/supp_comparisons}
\caption{Each row contains an input shape in the left column, followed by the output of each method: Ours, AEFA, DEFA, and EMAR (left to right). Each method was tested on over 4000 2D images from four datasets:  LEMS, MPEG-7, SiSHA, and PASCAL Horses.}
\label{supp_comparisons}
\end{figure}

{\small
\bibliographystyle{ieee}
\bibliography{egbib}
}


\end{document}
