\documentclass{bmvc2k}

\newcommand{\figref}[1]{Fig. \ref{#1}}
\newcommand{\tabref}[1]{Table \ref{#1}}
\newcommand{\secref}[1]{Sec. \ref{#1}}
\newcommand{\eqnref}[1]{Eqn. \ref{#1}} 
\newcommand{\algref}[1]{Alg. \ref{#1}}

%% Enter your paper number here for the review copy
\bmvcreviewcopy{884}

\title{Exploiting Protrusion Cues for Fast and Effective Shape Modeling via Ellipses \\
SUPPLEMENTARY MATERIALS}

% Enter the paper's authors in order
% \addauthor{Name}{email/homepage}{INSTITUTION_CODE}
\addauthor{Alex Wong}{alexw@cs.ucla.edu}{1}
\addauthor{Brian Taylor}{btay@cs.ucla.edu}{1}
\addauthor{Alan Yuille}{alan.yuille@jhu.edu}{2}

% Enter the institutions
% \addinstitution{Name\\Address}
\addinstitution{
 Department of Computer Science\\
 University of California, Los Angeles
}
\addinstitution{
 Department of Computer Science\\
 Johns Hopkins University
}

\runninghead{}{}

% Any macro definitions you would like to include
% These are not defined in the style file, because they don't begin
% with \bmva, so they might conflict with the user's own macros.
% The \bmvaOneDot macro adds a full stop unless there is one in the
% text already.
\def\eg{\emph{e.g}\bmvaOneDot}
\def\Eg{\emph{E.g}\bmvaOneDot}
\def\etal{\emph{et al}\bmvaOneDot}

%-------------------------------------------------------------------------
% Document starts here
\begin{document}

\maketitle


\begin{figure} [h!]
\centering
\setlength{\abovecaptionskip}{-12pt}
\setlength{\belowcaptionskip}{-12pt}
\includegraphics[width=1\textwidth]{figures/k_cuts_pipeline_square}
\caption{Examples of the local ellipse fitting procedure using $\kappa-1$ cuts. Given the intermediary results after applying protrusion cues, we first find the symmetric axis $\mathbf{A}_j$. We then perform $\kappa-1$ cuts along $\mathbf{A}_j$ to produce $\kappa$ regions. A candidate ellipse model is proposed for each value of $\kappa$, which typically ranges from 1 to 5. For each region, the local ellipse model that minimizes Eqn. 2 from the main paper is selected as the representation.}
\label{k_cuts_pipeline}
\end{figure}

\section{Local Ellipse Fitting with $\kappa-1$ cuts}
In the main paper, we described our local ellipse fitting method that performs $\kappa-1$ cuts along the symmetric axis of a region to produce $\kappa$ subregions, to each of which we fit an ellipse. We first find the longest continuous segment through the major axis of the region and denote it as the symmetric axis $\mathbf{A}_j$. Next, we initialize $\kappa$ to 1. The first iteration is, therefore, simply fitting a single ellipse over the entire region. We then increase $\kappa$ by 1 on each subsequent iteration to make $\kappa-1$ cuts along the $\mathbf{A}_j$. We compute the inner angle (Eqn. 3 from the main paper) formed at each point $a_o \in \mathbf{A}_j$ using the vectors $\vec{a_{po}}$ and $\vec{a_{qo}}$ where $a_p$ and $a_q$ are located $w$ pixels away on either side of $a_o$. We form cuts along $\mathbf{A}_j$ at locations where the inner angle is the sharpest (e.g. the value is smallest), resulting in $\kappa$ regions. We perform local ellipse fitting via a least-square estimator to generate a set of candidate ellipses to represent the regions. The procedure terminates when any of the ellipses falls outside of the image domain or when the least-square fitting fails due to matrix inversion. We then compute the ellipse fitting cost (Eqn. 2 from the main paper) for each set of candidate ellipses and select the model that minimizes the cost. This procedure is illustrated in \figref{k_cuts_pipeline}.

\begin{figure} [h!]
\centering
\setlength{\abovecaptionskip}{-12pt}
\setlength{\belowcaptionskip}{-12pt}
\includegraphics[width=0.90\textwidth]{figures/supp_comparisons}
\caption{Each row contains an input shape in the left column, followed by the output of each method: Ours, AEFA, DEFA, and EMAR (left to right). Note that our model is generally less complex and the ellipses produced by our model corresponds to semantic parts; whereas, the results of AEFA, DEFA and EMAR may not. This is due to their attempt to over fit the shape with additional ellipses, which not only loses semantic elements of the shape object, but also increasing the complexity of their models.}
\label{supp_comparisons}
\end{figure}

\section{Additional results}
In the experiment section, we reported results on the task of ellipse fitting on over 4000 2D shapes of different objects, sizes and orientations from four different datasets: LEMS, MPEG-7, SiSHA, and PASCAL Horses. Our shape coverage as measured by Intersection-Over-Union (IOU) is shown to be comparable to that of the Augmentative Ellipse Fitting Algorithm (AEFA) and the Decremental Ellipse Fitting Algorithm (DEFA) from \cite{panagiotakis2015parameter} and consistently outperforms the EM algorithm proposed by \cite{xu2010fitting}. Sample output from all of the methods evaluated in this paper is shown in \figref{supp_comparisons} for comparison. For the experiments, we allow each method to choose its own optimal number of ellipses (based on its model selection criterion) to fit a given shape. Due to similar initialization steps, our method generates results similar to that of AEFA and DEFA. However, our method is much faster than all of the competing methods, in particular AEFA and DEFA. This is due to the fact that AEFA and DEFA iteratively searches for the number of ellipses $n$ to fit a given shape along with the parameters for each ellipse, which translates into a large search space. Hence, their method incurs an extremely high time-complexity. Our method is able to approximate $n$ through the use of symmetry axis, protrusion cues and the $\kappa-1$-cuts procedure, allowing our approach to outperform them by a large margin in run-time. 


\bibliography{egbib}
\end{document}
